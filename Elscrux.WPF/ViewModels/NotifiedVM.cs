﻿using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Windows;
using DynamicData.Binding;
using Elscrux.Notification;
using Elscrux.WPF.Models;
using Noggog;
using Noggog.WPF;
using ReactiveUI;
namespace Elscrux.WPF.ViewModels; 

public class NotifiedVM : ViewModel, INotificationReceiver {
    public ObservableCollection<NotificationItem> LoadingItems { get; } = new();

    private readonly ObservableAsPropertyHelper<bool> _anyLoading;
    public bool AnyLoading => _anyLoading.Value;

    private readonly ObservableAsPropertyHelper<NotificationItem?> _latestNotification;
    public NotificationItem? LatestNotification => _latestNotification.Value;

    public NotifiedVM() {
        var observableLoadingItems = LoadingItems.ToObservableChangeSet();
        
        _anyLoading = observableLoadingItems
            .Select(x => LoadingItems.Count > 0)
            .ToProperty(this, x => x.AnyLoading);
        
        _latestNotification = observableLoadingItems
            .Select(x => LoadingItems.LastOrDefault())
            .ToProperty(this, x => x.LatestNotification);
    }
    
    public void ReceiveNotify(Guid id, string message) {
        var item = LoadingItems.FirstOrDefault(item => item.ID == id);
        if (item != null) {
            item.LoadText = message;
        } else {
            Application.Current.Dispatcher.Invoke(() => LoadingItems.Add(new NotificationItem(id, message, 0)));
        }
    }

    public void ReceiveProgress(Guid id, float progress) {
        var item = LoadingItems.FirstOrDefault(item => item.ID == id);
        if (item != null) {
            item.LoadProgress = progress;
        }
    }
    
    public void ReceiveStop(Guid id) {
        Application.Current.Dispatcher.Invoke(() => LoadingItems.RemoveWhere(x => x.ID == id));
    }
}
