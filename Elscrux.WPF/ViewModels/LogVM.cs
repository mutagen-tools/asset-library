﻿using System.Collections.ObjectModel;
using System.Reactive;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using Elscrux.Logging.Sinks;
using Elscrux.WPF.Models;
using Noggog.WPF;
using ReactiveUI;
using Serilog.Events;
namespace Elscrux.WPF.ViewModels;

public interface ILogVM : ILogListSink {
    public ReactiveCommand<Unit, Unit> ClearCommand { get; }

    public static Brush VerboseBrush { get; } = Brushes.CornflowerBlue;
    public static Brush DebugBrush { get; } = Brushes.ForestGreen;
    public static Brush MessageBrush { get; } = Brushes.White;
    public static Brush WarningBrush { get; } = Brushes.Yellow;
    public static Brush ErrorBrush { get; } =   Brushes.Red;
    public static Brush FatalBrush { get; } =   Brushes.DarkRed;
}

public class LogVM : ViewModel, ILogVM {
    private const int MaxLogCount = 500;

    public ObservableCollection<ILogItem> LogItems { get; set; } = new();
    
    public ReactiveCommand<Unit, Unit> ClearCommand { get; }

    public event ILogListSink.LogAddedHandler? OnLogAdded;

    public LogVM() {
        OnLogAdded += LimitLogCount;
        
        ClearCommand = ReactiveCommand.Create(Clear);
    }

    public void Emit(LogEvent logEvent) {
        var logMessage = logEvent.RenderMessage();
        AddText(logMessage, logEvent.Level);
    }

    private void AddText(string text, LogEventLevel level) {
        var logField = new LogItem(text, level);
        
        Application.Current.Dispatcher.Invoke(() => LogItems.Add(logField));
        OnLogAdded?.Invoke(logField);
    }

    private void Clear() => LogItems.Clear();

    private void LimitLogCount(ILogItem logItem) {
        Dispatcher.CurrentDispatcher.Invoke(() => {
            while (LogItems.Count > MaxLogCount) LogItems.RemoveAt(0);
        });
    }
}
