﻿using System.Reactive.Disposables;
using System.Windows.Controls;
using Noggog;
namespace Elscrux.WPF.ViewModels;

public class DisposableUserControl : UserControl, IDisposable, IDisposableDropoff {
    private readonly Lazy<CompositeDisposable> _compositeDisposable = new();

    public virtual void Dispose() {
        if (!_compositeDisposable.IsValueCreated)
            return;

        _compositeDisposable.Value.Dispose();
    }
    
    public void Add(IDisposable disposable) {
        _compositeDisposable.Value.Add(disposable);
    }
}
