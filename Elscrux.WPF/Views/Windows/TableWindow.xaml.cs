﻿using System.Dynamic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
namespace Elscrux.WPF.Views.Windows; 

public partial class TableWindow {
    public string WindowTitle { get; }

    public TableWindow(string windowTitle, List<(IReadOnlyList<string> columns, List<List<string>> rows)> dataGrids, List<(string, RoutedEventHandler)>? buttons = null) {
        InitializeComponent();
        
        WindowTitle = windowTitle;

        //Add data grids
        foreach (var (columns, rows) in dataGrids) {
            if (columns.Count <= 0 || rows.Count <= 0) continue;
            
            var dataGrid = new DataGrid {
                CanUserResizeRows = false,
                CanUserAddRows = false,
                CanUserDeleteRows = false,
                IsReadOnly = true,
                GridLinesVisibility = DataGridGridLinesVisibility.None,
                ContextMenu = new ContextMenu { Items = { ApplicationCommands.Copy } }
            };
            
            //Add columns
            foreach (var column in columns) {
                dataGrid.Columns.Add(new DataGridTextColumn {Header = column, Binding = new Binding(column)});
            }
            
            //Add rows
            for (var row = 0; row < rows.Max(list => list.Count); row++) {
                dynamic rowItem = new ExpandoObject();
                for (var column = 0; column < rows.Count && column < columns.Count; column++) {
                    if (row < rows[column].Count) {
                        ((IDictionary<string, object>)rowItem)[columns[column]] = rows[column][row];
                    }
                }
                
                dataGrid.Items.Add(rowItem);
            }

            var rowDefinition = new RowDefinition();
            MainGrid.RowDefinitions.Add(rowDefinition);
            MainGrid.Children.Add(dataGrid);
            Grid.SetRow(dataGrid, MainGrid.RowDefinitions.Count - 1);
        }
        
        //Add buttons
        if (buttons == null) return;

        var buttonGrid = new Grid();
        var rowDef = new RowDefinition { Height = GridLength.Auto };
        MainGrid.RowDefinitions.Add(rowDef);
        MainGrid.Children.Add(buttonGrid);
        Grid.SetRow(buttonGrid, MainGrid.RowDefinitions.Count - 1);
        
        foreach (var (name, function) in buttons) {
            var button = new Button { Content = name };
            button.Click += function;
            
            var columnDef = new ColumnDefinition();
            buttonGrid.ColumnDefinitions.Add(columnDef);
            buttonGrid.Children.Add(button);
            Grid.SetColumn(button, buttonGrid.ColumnDefinitions.Count - 1);
        }

        DataContext = this;
    }
}