﻿using System.Windows;
using System.Windows.Media;
namespace Elscrux.WPF.Views.Loading;

/// <summary>
/// Reusable loading spinner modified from here https://stackoverflow.com/questions/6359848/wpf-loading-spinner
/// </summary>
public partial class LoadSpinner {
    private bool _setup;
    
    public static readonly DependencyProperty LoadSpinnerTextProperty = DependencyProperty.Register(nameof(LoadSpinnerText), typeof(string), typeof(LoadSpinner), new PropertyMetadata(default(object)));
    public static readonly DependencyProperty IsLoadingProperty = DependencyProperty.Register(nameof(IsLoading), typeof(bool), typeof(LoadSpinner), new PropertyMetadata(default(bool)));
    
    public string LoadSpinnerText {
        get => (string) GetValue(LoadSpinnerTextProperty);
        set => SetValue(LoadSpinnerTextProperty, value);
    }

    public bool IsLoading {
        get => (bool) GetValue(IsLoadingProperty);
        set => SetValue(IsLoadingProperty, value);
    }
    
    public int SpinnerSize { get; set; } = 64;
    public int EllipseSize { get; set; } = 32;
    
    public Position EllipseNorth { get; private set; }
    public Position EllipseNorthEast { get; private set; }
    public Position EllipseEast { get; private set; }
    public Position EllipseSouthEast { get; private set; }
    public Position EllipseSouth { get; private set; }
    public Position EllipseSouthWest { get; private set; }
    public Position EllipseWest { get; private set; }
    public Position EllipseNorthWest { get; private set; }
    
    

    private const double AngleInRadians = 44.8;
    private static readonly double Cosine = Math.Cos(AngleInRadians);
    private static readonly double Sine = Math.Sin(AngleInRadians);

    public LoadSpinner() {
        InitializeComponent();
    }

    protected override void OnRender(DrawingContext drawingContext) {
        base.OnRender(drawingContext);

        if (!_setup) InitialSetup();
    }

    private void InitialSetup() {
        if (SpinnerSymbol == null) return;
        
        var horizontalCenter = SpinnerSymbol.RenderSize.Width / 2;
        var verticalCenter = SpinnerSymbol.RenderSize.Height / 2;
        var distance = SpinnerSize;
        
        EllipseNorth = new Position(horizontalCenter, verticalCenter - distance);
        EllipseNorthEast = new Position(horizontalCenter + distance * Cosine, verticalCenter - distance * Sine);
        EllipseEast = new Position(horizontalCenter + distance, verticalCenter);
        EllipseSouthEast = new Position(horizontalCenter + distance * Cosine, verticalCenter + distance * Sine);
        EllipseSouth = new Position(horizontalCenter, verticalCenter + distance);
        EllipseSouthWest = new Position(horizontalCenter - distance * Cosine, verticalCenter + distance * Sine);
        EllipseWest = new Position(horizontalCenter - distance, verticalCenter);
        EllipseNorthWest = new Position(horizontalCenter - distance * Cosine, verticalCenter - distance * Sine);
        
        _setup = true;
    }
}

public struct Position {
    public Position(double left, double top) {
        Left = left;
        Top = top;
    }
        
    public double Left { get; private set; }
    public double Top { get; private set; }
}