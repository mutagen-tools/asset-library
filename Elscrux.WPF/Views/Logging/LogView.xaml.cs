﻿using Elscrux.WPF.ViewModels;
namespace Elscrux.WPF.Views.Logging; 

public partial class LogView {
    public LogView(ILogVM logVM) {
        InitializeComponent();

        DataContext = logVM;
    }
}
