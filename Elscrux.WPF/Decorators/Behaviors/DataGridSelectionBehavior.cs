﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors;
using Microsoft.Xaml.Behaviors.Core;
using Noggog;
using ReactiveUI;
using EventTrigger = Microsoft.Xaml.Behaviors.EventTrigger;
namespace Elscrux.WPF.Decorators.Behaviors;

public class DataGridSelectionBehavior : Behavior<DataGrid> {
    public static readonly DependencyProperty AllCheckedProperty = DependencyProperty.Register(nameof(AllChecked), typeof(bool?), typeof(DataGridSelectionBehavior), new FrameworkPropertyMetadata(false));
    public static readonly DependencyProperty SelectionGuardProperty = DependencyProperty.Register(nameof(SelectionGuard), typeof(Func<ISelectable, bool>), typeof(DataGridSelectionBehavior), new FrameworkPropertyMetadata(new Func<ISelectable, bool>(_ => true)));

    private bool _isProcessing;

    public string? EnabledMapping { get; set; }

    public bool AddColumn { get; set; } = true;
    public bool AddCommands { get; set; } = true;
    public bool AddKeyBind { get; set; } = true;

    public Key KeyBindKey { get; set; } = Key.Space;
    public string KeyBindEventName { get; set; } = "KeyUp";

    public bool? AllChecked {
        get => (bool?) GetValue(AllCheckedProperty);
        set => SetValue(AllCheckedProperty, value);
    }

    public Func<ISelectable, bool> SelectionGuard {
        get => (Func<ISelectable, bool>) GetValue(SelectionGuardProperty);
        set => SetValue(SelectionGuardProperty, value);
    }

    protected override void OnAttached() {
        base.OnAttached();

        if (AddColumn) AddSelectionColumn();
        if (AddCommands) AddSelectionMenu();
        if (AddKeyBind) AddKeyBindings();
    }


    private void AddSelectionColumn() {
        var cellCheckBox = new FrameworkElementFactory(typeof(CheckBox));
        cellCheckBox.AddHandler(ToggleButton.CheckedEvent, new RoutedEventHandler((_, _) => UpdateAllChecked()));
        cellCheckBox.AddHandler(ToggleButton.UncheckedEvent, new RoutedEventHandler((_, _) => UpdateAllChecked()));
        cellCheckBox.SetValue(FrameworkElement.VerticalAlignmentProperty, VerticalAlignment.Center);
        cellCheckBox.SetValue(FrameworkElement.HorizontalAlignmentProperty, HorizontalAlignment.Center);
        cellCheckBox.SetBinding(ToggleButton.IsCheckedProperty, new Binding(nameof(ISelectable.IsSelected)) { UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });
        if (EnabledMapping != null) cellCheckBox.SetBinding(UIElement.IsEnabledProperty, new Binding(EnabledMapping) { UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

        var columnCheckBox = new FrameworkElementFactory(typeof(CheckBox));
        columnCheckBox.AddHandler(ToggleButton.CheckedEvent, new RoutedEventHandler((_, _) => SelectAllItems()));
        columnCheckBox.AddHandler(ToggleButton.UncheckedEvent, new RoutedEventHandler((_, _) => SelectAllItems(false)));
        columnCheckBox.SetBinding(ToggleButton.IsCheckedProperty, new Binding(nameof(AllChecked)) { UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged });

        var column = new DataGridTemplateColumn {
            HeaderTemplate = new DataTemplate(typeof(CheckBox)) { VisualTree = columnCheckBox },
            CellTemplate = new DataTemplate(typeof(CheckBox)) { VisualTree = cellCheckBox },
            CanUserResize = false,
            CanUserSort = false,
            CanUserReorder = false,
            Width = 40
        };

        AssociatedObject.Columns.Insert(0, column);
    }

    private void AddSelectionMenu() {
        AssociatedObject.ContextMenu ??= new ContextMenu();

        var selectAllMenu = new MenuItem {
            Header = "Select All",
            Command = ReactiveCommand.Create(() => SelectDynamic())
        };
        AssociatedObject.ContextMenu.Items.Insert(0, selectAllMenu);

        var invertMenu = new MenuItem {
            Header = "Invert",
            Command = ReactiveCommand.Create(InvertAll)
        };
        AssociatedObject.ContextMenu.Items.Insert(0, invertMenu);
    }

    private void AddKeyBindings() {
        var eventTrigger = new EventTrigger {
            EventName = KeyBindEventName,
            Actions = {
                new InvokeCommandAction {
                    Command = new ActionCommand(o => {
                        if (o is KeyEventArgs args && args.Key == KeyBindKey) {
                            ToggleSelection();
                        }
                    }),
                    PassEventArgsToCommand = true
                }
            }
        };

        Interaction.GetTriggers(AssociatedObject).Add(eventTrigger);
    }

    private void TryProcess(Action action) {
        if (_isProcessing) return;

        _isProcessing = true;
        action.Invoke();
        _isProcessing = false;
    }

    private void UpdateAllChecked() {
        TryProcess(() => {
            var totalCount = 0;
            var selectedCount = 0;
            foreach (var selectable in AssociatedObject.Items.Cast<ISelectable>()) {
                totalCount++;
                if (selectable.IsSelected) selectedCount++;
            }

            if (selectedCount == totalCount) {
                AllChecked = true;
            } else if (selectedCount > 0) {
                AllChecked = null;
            } else {
                AllChecked = false;
            }
        });
    }

    private void SelectSelectedItems(bool newState = true) {
        TryProcess(() => {
            _isProcessing = true;
            foreach (var selectedItem in AssociatedObject.SelectedItems) {
                var selectable = (ISelectable) selectedItem;
                selectable.IsSelected = newState && SelectionGuard.Invoke(selectable);
            }
        });

        UpdateAllChecked();
    }

    private void SelectAllItems(bool newState = true) {
        TryProcess(() => {
            foreach (var selectable in AssociatedObject.Items.Cast<ISelectable>()) {
                selectable.IsSelected = newState && SelectionGuard.Invoke(selectable);
                AllChecked = newState;
            }
        });
    }

    private void SelectDynamic(bool newState = true) {
        if (AssociatedObject.SelectedItems.Count > 1) {
            //Only select records in selection if multiple are selected
            SelectSelectedItems(newState);
        } else {
            //Otherwise select all records
            SelectAllItems(newState);
        }
    }

    private void ToggleSelection() {
        TryProcess(() => {
            _isProcessing = true;
            var newStatus = !AssociatedObject.SelectedItems
                .Cast<ISelectable>()
                .All(selectable => selectable.IsSelected);

            AssociatedObject.SelectedItems
                .Cast<ISelectable>()
                .ForEach(selectable => selectable.IsSelected = newStatus && SelectionGuard.Invoke(selectable));
        });

        UpdateAllChecked();
    }

    private void InvertAll() {
        TryProcess(() => {
            foreach (var selectable in AssociatedObject.Items.Cast<ISelectable>()) {
                selectable.IsSelected = !selectable.IsSelected && SelectionGuard.Invoke(selectable);
            }
        });

        UpdateAllChecked();
    }
}
