﻿namespace Elscrux.WPF.Decorators;

/// <summary>
/// Arguments for the Filter event.
/// </summary>
/// <remarks>
/// <p>The event receiver should set Accepted to true if the item
/// passes the filter, or false if it fails.</p>
/// </remarks>
public class StringFilterEventArgs : EventArgs {
    //------------------------------------------------------
    //
    //  Constructors
    //
    //------------------------------------------------------

    internal StringFilterEventArgs(object item, string filter) {
        Item = item;
        Filter = filter;
        Accepted = true;
    }

    //------------------------------------------------------
    //
    //  Public Properties
    //
    //------------------------------------------------------

    /// <summary>
    /// The object to be tested by the filter.
    /// </summary>
    public object Item { get; }
    
    /// <summary>
    /// The string that is is filtered.
    /// </summary>
    public string Filter { get; }

    /// <summary>
    /// The return value of the filter.
    /// </summary>
    public bool Accepted { get; set; }
}

/// <summary>
///     The delegate to use for handlers that receive FilterEventArgs.
/// </summary>
public delegate void StringFilterEventHandler(object sender, StringFilterEventArgs e);