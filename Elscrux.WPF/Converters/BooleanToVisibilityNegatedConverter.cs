﻿using System.Globalization;
using System.Windows;
using System.Windows.Data;
namespace Elscrux.WPF.Converters;

public class NegatedBooleanToVisibilityConverter : IValueConverter {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
        return value is true ? Visibility.Collapsed : Visibility.Visible;
    }
    
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
        return value is Visibility and not Visibility.Visible;
    }
}
