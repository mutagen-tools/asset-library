﻿using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
namespace Elscrux.WPF.Converters;

public class BooleanToColorConverter : IValueConverter {
    private static readonly Brush ValidBrush = Brushes.ForestGreen;
    private static readonly Brush ErrorBrush = Brushes.IndianRed;
    
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value is true ? ValidBrush : ErrorBrush;
    
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
        if (value is Brush brush) {
            return brush.Equals(ValidBrush);
        }

        return false;
    }
}
