﻿using System.Globalization;
using System.Windows.Data;
namespace Elscrux.WPF.Converters;

public class BooleanNegatedConverter : IValueConverter {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
        return Invert(value);
    }
    
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
        return Invert(value);
    }

    private static object Invert(object value) => value is not true;
}
