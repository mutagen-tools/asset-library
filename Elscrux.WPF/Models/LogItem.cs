﻿using System.Windows.Media;
using Elscrux.Logging.Sinks;
using Elscrux.WPF.ViewModels;
using Serilog.Events;
namespace Elscrux.WPF.Models;

public record LogItem(string Text, LogEventLevel Level) : ILogItem {
    public LogEventLevel Level { get; set; } = Level;
    public string Text { get; set; } = Text;
    
    public Brush Color => Level switch {
        LogEventLevel.Verbose => ILogVM.VerboseBrush,
        LogEventLevel.Debug => ILogVM.DebugBrush,
        LogEventLevel.Information => ILogVM.MessageBrush,
        LogEventLevel.Warning => ILogVM.WarningBrush,
        LogEventLevel.Error => ILogVM.ErrorBrush,
        LogEventLevel.Fatal => ILogVM.FatalBrush,
        _ => throw new ArgumentOutOfRangeException()
    };
}
