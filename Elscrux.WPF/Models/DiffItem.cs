﻿using System.IO;
using System.Windows.Media;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace Elscrux.WPF.Models;

public class DiffItem : ReactiveObject, ISelectable {
    public DiffItem(string oldEntry, string newEntry, bool isReadOnly = true) {
        Old = oldEntry;
        New = newEntry;
        IsReadOnly = isReadOnly;

        UpdateColor();
    }
    
    public DiffItem(string oldEntry, string newEntry, object data, bool isReadOnly = true) {
        Old = oldEntry;
        New = newEntry;
        Data = data;
        IsReadOnly = isReadOnly;

        UpdateColor();
    }

    public object? Data { get; protected set; }
    
    [Reactive]
    public bool IsSelected { get; set; }
    
    [Reactive]
    public string Old { get; set; }
    
    [Reactive]
    public string New { get; set; }
    
    [Reactive]
    public bool IsReadOnly { get; set; }
    
    [Reactive]
    public Brush Color { get; protected set; } = Brushes.Red;

    private static readonly Brush DefaultColor = Brushes.White;
    private static readonly Brush WarningColor = Brushes.Red;

    public void SetNewBaseDirectory(string baseDir) {
        New = baseDir == string.Empty ? Path.GetFileName(New) : Path.Combine(baseDir, Path.GetFileName(New));
        UpdateColor();
    }
    
    public void SetNewFile(string file) {
        New = file;
        UpdateColor();
    }

    public void UpdateColor() => Color = New != Old ? DefaultColor : WarningColor;
}
