﻿using Serilog.Events;
namespace Elscrux.Logging.Sinks;

public interface ILogItem {
    public string Text { get; set; }
    public LogEventLevel Level { get; set; }
}