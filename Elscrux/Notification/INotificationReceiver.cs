namespace Elscrux.Notification;

public interface INotificationReceiver {
    public void ReceiveNotify(Guid id, string message);
    public void ReceiveProgress(Guid id, float progress);
    public void ReceiveStop(Guid id);
}
