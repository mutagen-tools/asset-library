﻿using Autofac;
namespace Elscrux.Notification; 

public class NotificationModule : Module {
    protected override void Load(ContainerBuilder builder) {
        builder.RegisterType<Notifier>()
            .As<INotifier>()
            .SingleInstance();
    }
}
