﻿using Mutagen.Bethesda;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.Extension.Enum;
using Noggog;
namespace MutagenLibrary.Extension.Record; 

/// <summary>
/// Skyrim skill level
/// </summary>
public enum SkillLevel {
    Novice = 0,
    Apprentice = 25,
    Adept = 50,
    Expert = 75,
    Master = 100
}

public static class SpellExtension {
    /// <summary>
    /// Checks whether a spell has a scroll equivalent.
    /// This is checking if the spell's EditorID is a substring of the scroll's EditorID.
    /// </summary>
    /// <param name="spell">spell to use to check for a scroll</param>
    /// <param name="environment">environment to search for a scroll</param>
    /// <returns>true if a fitting scroll is found</returns>
    public static bool HasScroll(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment) {
        return spell.EditorID != null && environment.LinkCache.AllIdentifiers<IScrollGetter>().Any(scroll => scroll.EditorID != null && scroll.EditorID.Contains(spell.EditorID, StringComparison.OrdinalIgnoreCase));
    }
    
    /// <summary>
    /// Checks whether a spell has a staff equivalent.
    /// This is checking if the staff's EditorID is a substring of the staff's EditorID.
    /// </summary>
    /// <param name="spell">spell to use to check for a staff</param>
    /// <param name="environment">environment to search for a staff</param>
    /// <returns>true if a fitting staff is found</returns>
    public static bool HasStaff(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment) {
        return spell.EditorID != null && environment.LinkCache.AllIdentifiers<IWeaponGetter>().Any(weapon => weapon.EditorID != null && weapon.EditorID.Contains(spell.EditorID, StringComparison.OrdinalIgnoreCase));
    }
    
    /// <summary>
    /// Checks weather a spell has a right hand variant.
    /// Only checks for spells with EquipType BothHand or EitherHand.
    /// This is checking if any spell contains this spell's EditorID and has the RightHand EquipType.
    /// </summary>
    /// <param name="spell">spell to use to check for a right hand variant</param>
    /// <param name="environment">environment to search for a right hand variant</param>
    /// <returns>true if a fitting right hand variant is found</returns>
    public static bool HasRightHand(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment) => spell.HasXHand(environment, Skyrim.EquipType.RightHand.FormKey);

    /// <summary>
    /// Checks weather a spell has a left hand variant.
    /// Only checks for spells with EquipType BothHand or EitherHand.
    /// This is checking if any spell contains this spell's EditorID and has the LeftHand EquipType.
    /// </summary>
    /// <param name="spell">spell to use to check for a left hand variant</param>
    /// <param name="environment">environment to search for a left hand variant</param>
    /// <returns>true if a fitting left hand variant is found</returns>
    public static bool HasLeftHand(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment) => spell.HasXHand(environment, Skyrim.EquipType.LeftHand.FormKey);

    private static bool HasXHand(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment, FormKey equipType) {
        if (spell.EditorID == null || (spell.EquipmentType.FormKey != Skyrim.EquipType.EitherHand.FormKey && spell.EquipmentType.FormKey != Skyrim.EquipType.BothHands.FormKey)) return false;

        foreach (var s in environment.LinkCache.AllIdentifiers<ISpellGetter>()) {
            if (s.EditorID == null || !s.EditorID.Contains(spell.EditorID, StringComparison.OrdinalIgnoreCase)) continue;

            var spellRecord = environment.LinkCache.Resolve<ISpellGetter>(s.FormKey);

            if (spellRecord.EquipmentType.FormKey == equipType) {
                return true;
            }
        }

        return false;
    }

    
    /// <summary>
    /// Builds a scroll equivalent from a spell.
    /// The scroll's settings are based on the Skyrim scroll settings. Visuals are randomly chosen.
    /// </summary>
    /// <param name="spell">spell to base the new scroll on</param>
    /// <param name="environment">environment of the spell</param>
    /// <param name="mod">mod the scroll should be added to</param>
    /// <returns>the created scroll</returns>
    public static Scroll MakeScroll(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment, ISkyrimMod mod) {
        var spellSetter = spell.Duplicate(FormKey.Null);
        var scrollType = Random.Shared.Next(1, 7);
        
        var scroll = new Scroll(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
            EditorID = $"{spell.EditorID}Scroll",
            ObjectBounds = new ObjectBounds {
                First = scrollType switch {
                    1 => new P3Int16(-16, -4, -4),
                    >= 2 and <= 3 => new P3Int16(-15, -4, -3),
                    4 => new P3Int16(-9, -3, -2),
                    5 => new P3Int16(-15, -4, -4),
                    _ => new P3Int16(-13, -6, -3)
                },
                Second = scrollType switch {
                    1 => new P3Int16(16, 4, 4),
                    >= 2 and <= 3 => new P3Int16(15, 4, 3),
                    4 => new P3Int16(12, 3, 2),
                    5 => new P3Int16(17, 4, 4),
                    _ => new P3Int16(13, 6, 3)
                }
            },
            Name = $"Scroll of {spell.Name}",
            Keywords = new ExtendedList<IFormLinkGetter<IKeywordGetter>> { Skyrim.Keyword.VendorItemScroll },
            MenuDisplayObject = Skyrim.Static.HighPolyBookTest01.AsNullable(),
            EquipmentType = spellSetter.EquipmentType,
            Description = spellSetter.Description,
            Model = new Model {
                File = $"Clutter\\Common\\Scroll0{scrollType}.nif"
            },
            Weight = 0.5f,
            Value = spell.GetSkillLevel(environment) switch {
                SkillLevel.Novice => 25,
                SkillLevel.Apprentice => 50,
                SkillLevel.Adept => 100,
                SkillLevel.Expert => 250,
                SkillLevel.Master => 500,
                _ => 0
            },
            BaseCost = spell.BaseCost,
            Flags = spell.Flags,
            Type = spell.Type,
            ChargeTime = spell.ChargeTime,
            CastType = CastType.Scroll,
            TargetType = spell.TargetType,
            CastDuration = spell.CastDuration,
            Range = spell.Range,
            HalfCostPerk = new FormLinkNullable<IPerkGetter>(),
            Effects = spellSetter.Effects
        };

        return scroll;
    }

    /// <summary>
    /// Builds a staff equivalent from a spell.
    /// The staff's settings are based on the Skyrim staff settings. Visuals are based on the spell's magic school.
    /// </summary>
    /// <param name="spell">spell to base the new staff on</param>
    /// <param name="environment">environment of the spell</param>
    /// <param name="mod">mod the staff should be added to</param>
    /// <param name="prefix">prefix of the spell's EditorID, if it has any. (Example: DLC1, DLC2, BYOH)</param>
    /// <returns>the created staff</returns>
    public static Weapon MakeStaff(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment, ISkyrimMod mod, string prefix) {
        var spellSetter = spell.Duplicate(FormKey.Null);

        var skill = spell.GetSkillLevel(environment);
        var school = spell.GetSchool(environment);
        var objectEffect = spell.MakeStaffEnchantment(mod, prefix);
        mod.ObjectEffects.Add(objectEffect);
        
        var staff = new Weapon(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
            EditorID = $"{prefix}Staff{spell.EditorID?.TrimStart(prefix)}",
            ObjectBounds = new ObjectBounds {
                First = school switch {
                    ActorValue.Destruction => new P3Int16(-6, -69, -2),
                    ActorValue.Conjuration => new P3Int16(-5, -73, -9),
                    ActorValue.Alteration => new P3Int16(-3, -50, -3),
                    ActorValue.Illusion => new P3Int16(-3, -72, -2),
                    _ => new P3Int16(-6, -67, -5)
                },
                Second = school switch {
                    ActorValue.Destruction => new P3Int16(6, 64, 2),
                    ActorValue.Conjuration => new P3Int16(10, 68, 9),
                    ActorValue.Alteration => new P3Int16(3, 31, 3),
                    ActorValue.Illusion => new P3Int16(3, 57, 2),
                    _ => new P3Int16(4, 67, 5)
                }
            },
            Name = $"Staff of {spell.Name}",
            Model = new Model {
                File = school switch {
                    ActorValue.Destruction => @"Weapons\DragonPriest\DragonPriestStaff3rd.nif",
                    ActorValue.Conjuration => @"Weapons\Staff04\Staff04.nif",
                    ActorValue.Alteration => @"Weapons\Staff03\Staff03.nif",
                    ActorValue.Illusion => @"Weapons\Staff02\Staff02.nif",
                    _ => @"Weapons\Staff01\Staff01.nif"
                }
            },
            ObjectEffect = new FormLinkNullable<IEffectRecordGetter>(objectEffect),
            EnchantmentAmount = skill switch {
                SkillLevel.Novice => 500,
                SkillLevel.Apprentice => 1000,
                SkillLevel.Adept => 2000,
                SkillLevel.Expert => 3000,
                SkillLevel.Master => 5000,
                _ => 0
            },
            EquipmentType = spellSetter.EquipmentType,
            BlockBashImpact = Skyrim.ImpactDataSet.WPNBashBowImpactSet.AsNullable(),
            AlternateBlockMaterial = Skyrim.MaterialType.MaterialBlockBowsStaves.AsNullable(),
            Keywords = new ExtendedList<IFormLinkGetter<IKeywordGetter>> { Skyrim.Keyword.WeapTypeStaff, Skyrim.Keyword.VendorItemStaff },            
            Description = spellSetter.Description,
            FirstPersonModel = school switch {
                ActorValue.Destruction => Skyrim.Static._1stPersonDragonPriestStaff.AsNullable(),
                ActorValue.Conjuration => Skyrim.Static._1stPersonStaff04.AsNullable(),
                ActorValue.Alteration => Skyrim.Static._1stPersonStaff03.AsNullable(),
                ActorValue.Illusion => Skyrim.Static._1stPersonStaff02.AsNullable(),
                _ => Skyrim.Static._1stPersonStaff01.AsNullable()
            },
            AttackFailSound = Skyrim.SoundDescriptor.WPNSwingBladeMediumSD.AsNullable(),
            EquipSound = Skyrim.SoundDescriptor.WPNStaffHandDrawSD.AsNullable(),
            UnequipSound = Skyrim.SoundDescriptor.WPNStaffHandSheatheSD.AsNullable(),
            BasicStats = new WeaponBasicStats {
                Value = 5,
                Weight = 8,
                Damage = 0
            },
            Data = new WeaponData {
                AnimationType = WeaponAnimationType.Staff,
                Speed = 1,
                Reach = 1.3f,
                SightFOV = 0,
                BaseVATStoHitChance = 0,
                NumProjectiles = 1,
                EmbeddedWeaponAV = 0,
                RangeMin = 0,
                RangeMax = 0,
                OnHit = WeaponData.OnHitType.NoFormulaBehavior,
                AnimationAttackMult = 1,
                RumbleLeftMotorStrength = 1,
                RumbleRightMotorStrength = 1,
                RumbleDuration = 0.33f,
                Skill = EnumConverter.ActorValueToSkill(school),
                Resist = ActorValue.None,
                Stagger = 0
            },
            Critical = new CriticalData {
                Damage = 0,
                PercentMult = 1,
                Flags = CriticalData.Flag.OnDeath,
                Effect = new FormLinkNullable<ISpellGetter>()
            },
            DetectionSoundLevel = SoundLevel.Normal,
            Template = school switch { 
                ActorValue.Destruction => Skyrim.Weapon.StaffTemplateDestruction.AsNullable(),
                ActorValue.Conjuration => Skyrim.Weapon.StaffTemplateConjuration.AsNullable(),
                ActorValue.Alteration => Skyrim.Weapon.StaffTemplateAlteration.AsNullable(),
                ActorValue.Illusion => Skyrim.Weapon.StaffTemplateIIllusion.AsNullable(),
                _ => Skyrim.Weapon.StaffTemplateRestoration.AsNullable()
            }
        };

        return staff;
    }
    
    /// <summary>
    /// Builds a right hand variant of a spell.
    /// </summary>
    /// <param name="spell">spell to base the new right hand variant on</param>
    /// <param name="mod">mod the right hand variant should be added to</param>
    /// <returns>created right hand variant</returns>
    public static Spell MakeRightHandVariant(this ISpellGetter spell, ISkyrimMod mod) {
        var rightHandSpell = spell.Duplicate(mod.GetNextFormKey());
        rightHandSpell.EditorID = $"{spell.EditorID}RightHand";
        rightHandSpell.EquipmentType = Skyrim.EquipType.RightHand.AsNullable();
        
        return rightHandSpell;
    }

    /// <summary>
    /// Builds a left hand variant of a spell.
    /// </summary>
    /// <param name="spell">spell to base the new left hand variant on</param>
    /// <param name="mod">mod the left hand variant should be added to</param>
    /// <returns>created left hand variant</returns>
    public static Spell MakeLeftHandVariant(this ISpellGetter spell, ISkyrimMod mod) {
        var rightHandSpell = spell.Duplicate(mod.GetNextFormKey());
        rightHandSpell.EditorID = $"{spell.EditorID}LeftHand";
        rightHandSpell.EquipmentType = Skyrim.EquipType.LeftHand.AsNullable();
        
        return rightHandSpell;
    }
    
    /// <summary>
    /// Builds a staff enchantment equivalent from a spell.
    /// </summary>
    /// <param name="spell">spell to base the new staff enchantment on</param>
    /// <param name="mod">mod the left hand variant should be added to</param>
    /// <param name="prefix">prefix of the spell's EditorID, if it has any (Example: DLC1, DLC2, BYOH)</param>
    /// <returns></returns>
    public static ObjectEffect MakeStaffEnchantment(this ISpellGetter spell, ISkyrimMod mod, string prefix) {
        var spellSetter = spell.Duplicate(FormKey.Null);
        
        var staff = new ObjectEffect(mod.GetNextFormKey(), SkyrimRelease.SkyrimSE) {
            EditorID = $"{prefix}StaffEnch{spell.EditorID?.TrimStart(prefix)}",
            Name = spellSetter.Name,
            EnchantmentCost = spell.BaseCost,
            CastType = spell.CastType,
            EnchantmentAmount = Convert.ToInt32(spell.BaseCost),
            TargetType = spell.TargetType,
            EnchantType = ObjectEffect.EnchantTypeEnum.StaffEnchantment,
            ChargeTime = spell.ChargeTime,
            BaseEnchantment = new FormLinkNullable<IObjectEffectGetter>(),
            WornRestrictions = new FormLinkNullable<IFormListGetter>(),
            Effects = spellSetter.Effects
        };

        return staff;
    }
    
    /// <summary>
    /// Tries to determine the spell's skill level.
    /// This is checking the highest skill requirement of any of the spell's effects and translates it to skill levels.
    /// </summary>
    /// <param name="spell">spell to check skill for</param>
    /// <param name="environment">environment the spell is in</param>
    /// <returns>skill level of the spell as SkillLevel enum</returns>
    public static SkillLevel GetSkillLevel(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment) {
        uint highestLevel = 0;
        foreach (var effect in spell.Effects) {
            var magicEffect = effect.BaseEffect.TryResolve(environment.LinkCache);
            if (magicEffect == null) continue;

            if (magicEffect.MinimumSkillLevel >= highestLevel) {
                highestLevel = magicEffect.MinimumSkillLevel;
            }
        }

        return highestLevel switch {
            < 25 => SkillLevel.Novice,
            < 50 => SkillLevel.Apprentice,
            < 75 => SkillLevel.Adept,
            < 100 => SkillLevel.Expert,
            >= 100 => SkillLevel.Master
        };
    }
    
    /// <summary>
    /// Tries to determine the spell's magic school.
    /// It returns the first magic skill of the spell's effects.
    /// </summary>
    /// <param name="spell">spell to check skill for</param>
    /// <param name="environment">environment the spell is in</param>
    /// <returns>school of the spell as ActorValue</returns>
    public static ActorValue GetSchool(this ISpellGetter spell, IGameEnvironment<ISkyrimMod, ISkyrimModGetter> environment) {
        foreach (var effect in spell.Effects) {
            var magicEffect = effect.BaseEffect.TryResolve(environment.LinkCache);
            if (magicEffect == null || magicEffect.MagicSkill == ActorValue.None) continue;

            return magicEffect.MagicSkill;
        }

        return ActorValue.None;
    }
}