﻿using Mutagen.Bethesda.FormKeys.SkyrimSE;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Aspects;
using Mutagen.Bethesda.Plugins.Cache;
using Mutagen.Bethesda.Skyrim;
using MutagenLibrary.References.ReferenceCache;
namespace MutagenLibrary.Extension.Record;

public static class ObjectEffectExtension {
    /// <summary>
    /// Recursively retrieves the worn restriction of an object effect
    /// If the object effect doesn't worn restriction, it tries to return its parent's worn restrictions
    /// </summary>
    /// <param name="objectEffect">object effect to retrieve the worn restrictions for</param>
    /// <param name="linkCache">link cache to retrieve parent object effects</param>
    /// <returns></returns>
    public static IFormLinkGetter<IFormListGetter>? GetWornRestrictions(this IObjectEffectGetter objectEffect, ILinkCache linkCache) {
        return !objectEffect.WornRestrictions.IsNull ? objectEffect.WornRestrictions : objectEffect.BaseEnchantment.TryResolve(linkCache)?.GetWornRestrictions(linkCache);
    }

    /// <summary>
    /// Checks if an enchantment can be used by the player, meaning that
    /// it can be disenchanted from a weapons/armor and freely re-enchanted on other weapons/armor
    /// </summary>
    /// <param name="objectEffect">enchantment to check for</param>
    /// <param name="linkCache">link cache of existing weapons/armor to check for</param>
    /// <param name="referenceQuery">reference query to check for weapons/armor referencing object effects</param>
    /// <returns>true if the enchantment is playable</returns>
    public static bool IsPlayableEnchantment(this IObjectEffectGetter objectEffect, ILinkCache linkCache, ReferenceQuery referenceQuery) {
        referenceQuery.LoadModReferences(linkCache);
        
        var wornRestrictions = objectEffect.GetWornRestrictions(linkCache)?.TryResolve(linkCache);
        foreach (var formLink in referenceQuery.GetReferences(objectEffect.FormKey)) {
            IKeywordedGetter record;

            if (formLink.Type == typeof(IArmorGetter) && linkCache.TryResolve<IArmorGetter>(formLink.FormKey, out var armor)) {
                record = armor;
            } else if (formLink.Type == typeof(IWeaponGetter) && linkCache.TryResolve<IWeaponGetter>(formLink.FormKey, out var weapon)) {
                record = weapon;
            } else {
                continue;
            }

            //If one item doesn't have any keywords, then it can be disenchanted
            if (record.Keywords == null) return true;
            
            if (wornRestrictions == null) {
                //Check if any referenced item doesn't have the magic disallow enchanting keyword
                if (record.Keywords == null || record.Keywords.Any(k => k.FormKey == Skyrim.Keyword.MagicDisallowEnchanting.FormKey)) continue;
            } else {
                var restrictionKeywords = wornRestrictions.Items.Select(i => i.FormKey).ToList();

                //Check if any referenced item doesn't have the magic disallow enchanting keyword and has at least one worn restriction keyword
                if (record.Keywords == null
                 || record.Keywords.Any(k => k.FormKey == Skyrim.Keyword.MagicDisallowEnchanting.FormKey)
                 || !record.Keywords.Any(k => restrictionKeywords.Contains(k.FormKey))) continue;
            }

            if (record.Keywords == null || record.Keywords.Any(k => k.FormKey == Skyrim.Keyword.MagicDisallowEnchanting.FormKey)) continue;

            return true;
        }

        return false;
    }
    
    public static bool IsInEnchantingSublist(this IEnchantableGetter enchantable, ILinkCache linkCache, ReferenceQuery referenceQuery, int tier) {
        if (enchantable.EditorID == null) return false;
        
        referenceQuery.LoadModReferences(linkCache);

        foreach (var formLink in referenceQuery.GetReferences(enchantable.FormKey)) {
            if (formLink.Type == typeof(ILeveledItemGetter)) {
                var leveledItem = linkCache.Resolve<ILeveledItemGetter>(formLink.FormKey);

                switch (enchantable) {
                    case IArmorGetter armor:
                        var baseArmor = armor.TemplateArmor.TryResolve(linkCache);
                        if (baseArmor != null && leveledItem.IsArmorSublist(baseArmor, tier)) {
                            return true;
                        }
                        break;
                    case IWeaponGetter weapon when leveledItem.IsWeaponSublist(weapon):
                        return true;
                }
            }
        }

        return false;
    }

    public static bool IsStaffEnchantment(this IObjectEffectGetter objectEffect) {
        return objectEffect.EnchantType == ObjectEffect.EnchantTypeEnum.StaffEnchantment;
    }
    
    public static bool IsWeaponEnchantment(this IObjectEffectGetter objectEffect) {
        return objectEffect.CastType is CastType.FireAndForget or CastType.Concentration && objectEffect.TargetType == TargetType.Touch;
    }
    
    public static bool IsArmorEnchantment(this IObjectEffectGetter objectEffect) {
        return objectEffect.CastType == CastType.ConstantEffect && objectEffect.TargetType == TargetType.Self;
    }
    
    /// <summary>
    /// Check if any referenced items doesn't have the magic disallow enchanting keyword
    /// </summary>
    /// <param name="objectEffect">object effect to check for</param>
    /// <param name="linkCache">link cache to resolve references</param>
    /// <param name="referenceQuery">reference query to search in</param>
    /// <returns></returns>
    public static bool CanBeDisenchanted(this IObjectEffectGetter objectEffect, ILinkCache linkCache, ReferenceQuery referenceQuery) {
        referenceQuery.LoadModReferences(linkCache);
        
        foreach (var formLink in referenceQuery.GetReferences(objectEffect.FormKey)) {
            IKeywordedGetter record;

            if (formLink.Type == typeof(IArmorGetter) && linkCache.TryResolve<IArmorGetter>(formLink.FormKey, out var armor)) {
                record = armor;
            } else if (formLink.Type == typeof(IWeaponGetter) && linkCache.TryResolve<IWeaponGetter>(formLink.FormKey, out var weapon)) {
                record = weapon;
            } else {
                continue;
            }

            if (record.Keywords == null || record.Keywords.Any(k => k.FormKey == Skyrim.Keyword.MagicDisallowEnchanting.FormKey)) continue;

            return true;
        }

        return false;
    }
}
