﻿using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using MutagenLibrary.WPF.SF.Json.Editor;
namespace MutagenLibrary.WPF.SF.Json.Presets;

public abstract class BasicPresetsConfigurator : UserControl {
    public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(nameof(IsReadOnly), typeof(bool), typeof(BasicPresetsConfigurator), new PropertyMetadata(false));
    
    public bool IsReadOnly {
        get => (bool) GetValue(IsReadOnlyProperty);
        set => SetValue(IsReadOnlyProperty, value);
    }
    
    public abstract string Header { get; set; }

    public BasicPresetsConfigurator() {
        SetResourceReference(StyleProperty, "BasicConfiguratorStyle");
    }
}

public abstract class BasicPresetsConfigurator<TPresetData, TPresetManager, TDataEditor> : BasicPresetsConfigurator
    where TPresetData : class, new()
    where TPresetManager : BasicPresetManager<TPresetData, TPresetManager>
    where TDataEditor : JsonEditor, new() {
    
    private readonly TPresetManager _presetManager = (typeof(TPresetManager).GetField("Instance", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy)!.GetValue(null) as Lazy<TPresetManager>)!.Value;
    
    public TDataEditor Editor { get; set; } = new();
    
    public BasicPresetsConfigurator() {
        Editor.DataContext = this;
        Editor.SetValue(JsonEditor.JsonFileProperty, _presetManager.ActivePresetFile);
        Editor.SetBinding(JsonEditor.ReadOnlyProperty, new Binding(nameof(IsReadOnly)));
        
        Editor.JsonSaved += OnJsonSaved;
    }
    
    protected virtual void OnJsonSaved(object sender, RoutedEventArgs e) {
        _presetManager.UpdateActivePreset();
    }
}
