﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using Newtonsoft.Json;
using Noggog;
using Syncfusion.Data.Extensions;
namespace MutagenLibrary.WPF.SF.Json.Presets;

public class Preset<TData> {
    public Preset(string name, TData data) {
        Name = name;
        Data = data;
    }

    public string Name { get; set; }
    public TData Data { get; set; }
}

public abstract class PresetManager<TData, TSelf> : BasicPresetManager<TData, TSelf>
    where TData : class, new()
    where TSelf : PresetManager<TData, TSelf> {
    private readonly string _baseDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Presets");

    public ObservableCollection<string> PresetFileNames { get; set; }
    private new string PresetsDirectoryPath => Path.Combine(_baseDirectory, PresetsDirectoryName);

    private readonly FileSystemWatcher FileSystemWatcher;

    protected PresetManager() {
        //Try to load default json
        var fileInfo = new FileInfo(GetPresetFilePath(DefaultName));
        fileInfo.Directory?.Create();
        File.WriteAllText(fileInfo.FullName, JsonConvert.SerializeObject(DefaultData, Formatting.Indented));
        _activePreset = new Preset<TData>(DefaultName, DefaultData);

        //Get existing files
        PresetFileNames = Directory.GetFiles(PresetsDirectoryPath, FileFilter)
            .Select(Path.GetFileNameWithoutExtension).NotNull()
            .ToObservableCollection();

        //Init file system watcher
        FileSystemWatcher = new FileSystemWatcher(PresetsDirectoryPath, FileFilter) {
            IncludeSubdirectories = false,
            EnableRaisingEvents = true
        };
        FileSystemWatcher.Created += (_, args) => FileSystemWatcherOnCreated(args.Name);
        FileSystemWatcher.Deleted += (_, args) => FileSystemWatcherOnDeleted(args.Name);
        FileSystemWatcher.Renamed += FileSystemWatcherOnRenamed;
    }

    #region Preset Configuration
    /// <summary>
    /// Sets the preset with the specified name as new active preset
    /// The preset will be loaded if available or a new created preset will be created
    /// In case of an error, the default preset will be set
    /// </summary>
    /// <param name="name">Preset name of the new active preset</param>
    public void SetPreset(string? name = default) {
        if (name.IsNullOrWhitespace() || name == DefaultName) {
            SetDefaultPreset();
        } else if (File.Exists(GetPresetFilePath(name))) {
            LoadPreset(name);
        } else {
            AddPreset(name);
        }
    }

    /// <summary>
    /// Loads an existing preset and sets it as the active preset
    /// </summary>
    /// <param name="name"></param>
    private void LoadPreset(string name) {
        var data = LoadData(name);
        if (data != null) {
            ActivePreset = new Preset<TData>(name, data);
        }
    }

    /// <summary>
    /// Adds a new preset and sets it as the active preset
    /// </summary>
    /// <param name="name"></param>
    private void AddPreset(string name) {
        if (name.IsNullOrWhitespace()) return;

        if (PresetFileNames.Contains(name)) return;

        ActivePreset = new Preset<TData>(name, new TData());
        PresetFileNames.Add(name);

        var fileInfo = new FileInfo(GetPresetFilePath(name));
        fileInfo.Directory?.Create();
        File.WriteAllText(fileInfo.FullName, JsonConvert.SerializeObject(DefaultData, Formatting.Indented));
    }

    /// <summary>
    /// Removes the currently active preset and sets the default preset afterwards
    /// </summary>
    public void RemoveActivePreset() {
        if (IsDefaultPreset()) return;

        var presetFilePath = GetPresetFilePath(ActivePreset.Name);
        if (File.Exists(presetFilePath)) File.Delete(presetFilePath);
        PresetFileNames.Remove(ActivePreset.Name);

        SetDefaultPreset();
    }
    
    public bool HasPreset(string name) => name != DefaultName && PresetFileNames.Contains(name);
    #endregion

    #region Default Preset
    /// <summary>
    /// Checks if the given string matches the name of the default preset
    /// </summary>
    /// <param name="name">string to match default preset</param>
    /// <returns>true if the given string matches the default preset</returns>
    public bool IsDefaultPreset(string name) => name == DefaultName;

    /// <summary>
    /// Checks if the active preset is the default preset
    /// </summary>
    /// <returns>true if the active preset is the default preset</returns>
    public bool IsDefaultPreset() => IsDefaultPreset(ActivePreset.Name);

    /// <summary>
    /// Sets the default preset as the active preset
    /// </summary>
    public void SetDefaultPreset() {
        if (IsDefaultPreset()) return;

        LoadPreset(DefaultName);
    }
    #endregion

    #region File System Watcher
    private void FileSystemWatcherOnCreated(string? file) {
        if (file == null || Path.GetExtension(file) != FileExtension) return;

        var name = Path.GetFileNameWithoutExtension(file);
        if (!PresetFileNames.Contains(name)) {
            Application.Current.Dispatcher.Invoke(() => PresetFileNames.Add(name));
        }
    }

    private void FileSystemWatcherOnDeleted(string? file) {
        if (file == null || Path.GetExtension(file) != FileExtension) return;

        Application.Current.Dispatcher.Invoke(() => {
            var fileName = Path.GetFileNameWithoutExtension(file);
            if (!IsDefaultPreset(fileName)) {
                PresetFileNames.Remove(fileName);
            }
        });
    }

    private void FileSystemWatcherOnRenamed(object sender, RenamedEventArgs e) {
        FileSystemWatcherOnDeleted(e.OldName);
        FileSystemWatcherOnCreated(e.Name);

        if (e.Name != null && ActivePreset.Name == e.OldName) {
            LoadPreset(e.Name);
        }
    }
    #endregion
}
