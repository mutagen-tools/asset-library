﻿using System.IO;
using System.Windows;
using Newtonsoft.Json;
using Noggog.WPF;
namespace MutagenLibrary.WPF.SF.Json.Presets;

public abstract class BasicPresetManager<TData, TSelf> : ViewModel
    where TData : class, new()
    where TSelf : BasicPresetManager<TData, TSelf> {
    public static readonly Lazy<TSelf> Instance = new();

    protected const string FileExtension = ".json";
    protected const string FileFilter = $"*{FileExtension}";

    protected abstract string PresetsDirectoryName { get; }
    protected string PresetsDirectoryPath => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, PresetsDirectoryName);

    public virtual string DefaultName => "Default";
    protected abstract TData DefaultData { get; }

    public static TData Data => Instance.Value.ActivePreset.Data;

    protected Preset<TData> _activePreset;
    public Preset<TData> ActivePreset {
        get => _activePreset;
        protected set {
            _activePreset = value;
            PresetChanged?.Invoke(this, new RoutedEventArgs());
        }
    }
    public string ActivePresetFile => GetPresetFilePath(ActivePreset.Name);
    
    public event RoutedEventHandler? PresetChanged;

    protected BasicPresetManager() {
        //Try to load json, otherwise use default
        var fileInfo = new FileInfo(GetPresetFilePath(DefaultName));
        fileInfo.Directory?.Create();
        if (File.Exists(fileInfo.FullName)) {
            _activePreset = new Preset<TData>(DefaultName, LoadData(DefaultName) ?? DefaultData);
        } else {
            File.WriteAllText(fileInfo.FullName, JsonConvert.SerializeObject(DefaultData, Formatting.Indented));
            _activePreset = new Preset<TData>(DefaultName, DefaultData);
        }
    }

    #region Preset Configuration
    /// <summary>
    /// Loads an existing preset and sets it as the active preset
    /// </summary>
    /// <param name="name"></param>
    private void LoadPreset(string name) {
        var data = LoadData(name);
        if (data != null) {
            ActivePreset = new Preset<TData>(name, data);
        }
    }
    
    public void UpdateActivePreset() {
        LoadPreset(ActivePreset.Name);
    }
    #endregion
    
    #region Utility
    /// <summary>
    /// Loads an existing preset and sets it as the active preset
    /// </summary>
    /// <param name="name"></param>
    protected TData? LoadData(string name) {
        try {
            var jsonText = File.ReadAllText(GetPresetFilePath(name));
            return JsonConvert.DeserializeObject<TData>(jsonText);
        } catch (JsonException) {
            return null;
        }
    }
    
    public string GetPresetFilePath(string name) => Path.Combine(PresetsDirectoryPath, name + FileExtension);
    #endregion
}
