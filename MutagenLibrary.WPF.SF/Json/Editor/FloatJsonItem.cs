﻿using System.Reflection;
using MutagenLibrary.WPF.SF.Json.Editor.View;
using Newtonsoft.Json.Linq;
using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public class FloatJsonItem : JsonItem<float> {
    public FloatJsonItem(JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) : base(typeof(float), token, parent, memberInfo) {
        Value = token.ToObject<float>();
        View = new FloatJsonItemView(this);
        
        this.WhenAnyValue(x => x.Value)
            .Subscribe(_ => {
                if (JsonValue == null) return;

                if (Token is JValue value) value.Value = JsonValue;
            });
    }
}
