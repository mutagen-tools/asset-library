﻿using System.Collections;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows.Controls;
using Mutagen.Bethesda.WPF.Reflection;
using Mutagen.Bethesda.WPF.Reflection.Attributes;
using Newtonsoft.Json.Linq;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using Syncfusion.Data.Extensions;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public interface IJsonItem {
    public UserControl? View { get; set; }
    
    public string? CustomTooltip { get; set; }
    
    public JToken Token { get; set; }
    public string Path { get; set; }
    public Type Type { get; set; }
    public ObservableCollection<IJsonItem> ChildrenItems { get; set; }
    public IJsonItem? ParentItem { get; set; }
    
    public void Update() {
        Path = Token.Path.Split('.').Last();
    }
    
    public void Remove() {
        Token.Remove();

        if (ParentItem != null) {
            ParentItem.ChildrenItems.Remove(this);

            ParentItem.Update();
            FunctionalExtensions.ForEach(ParentItem.ChildrenItems, c => c.Update());
        }
    }
    
    public void Clear() {
        foreach (var child in ChildrenItems) child.Token.Remove();
        ChildrenItems.Clear();
    }
    
    public bool IsCollection() => JTokenExtension.IsCollection(Type) && Token.Type is JTokenType.Array;
    
    public static IJsonItem Factory(Type type, JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) {
        //Skip single field objects
        while (token.Type == JTokenType.Object && token is { First: {} } && type.GetFields().Length == 1) {
            type = type.GetFields()[0].FieldType;
            token = token.First;
            if (token is JProperty property) token = property.Value;
        }
        
        switch (token) {
            case { Type: JTokenType.Boolean }:
                return new BooleanJsonItem(token, parent, memberInfo);
            case { Type: JTokenType.Integer }:
                return new IntegerJsonItem(token, parent, memberInfo);
            case { Type: JTokenType.Float }:
                return new FloatJsonItem(token, parent, memberInfo);
            case { Type: JTokenType.String }:
                return type.Name.Contains("FormLink") ? new FormKeyJsonItem(type, token, parent, memberInfo) : new StringJsonItem(token, parent, memberInfo);
            
            case { Type: JTokenType.Array }:
                var arrayJsonItem = new EmptyJsonItem(type, token, parent, memberInfo);
                foreach (var subToken in token) {
                    var fieldType = subToken.GetFieldType(type, out var fieldInfo);
                    arrayJsonItem.ChildrenItems.Add(Factory(fieldType, subToken, arrayJsonItem, fieldInfo));
                }

                return arrayJsonItem;
            case { Type: JTokenType.Object }:
                var objectJsonItem = new EmptyJsonItem(type, token, parent, memberInfo);
            
                foreach (var subToken in token) {
                    var fieldType = subToken.GetFieldType(type, out var fieldInfo);
                    
                    switch (subToken) {
                        case JProperty jProperty:
                            objectJsonItem.ChildrenItems.Add(Factory(fieldType, jProperty.Value, objectJsonItem, fieldInfo));
                            break;
                        default: 
                            objectJsonItem.ChildrenItems.Add(Factory(fieldType, subToken, objectJsonItem, fieldInfo));
                            break;
                    }
                }
                
                return objectJsonItem;
            default:
                throw new ArgumentOutOfRangeException(nameof(token));
        }
    }
}
public abstract class JsonItem<TType> : ReactiveObject, IJsonItem {
    [Reactive]
    public TType? Value { get; set; }
    public virtual object? JsonValue => Value;

    public string? CustomTooltip { get; set; }

    public UserControl? View { get; set; }
    public JToken Token { get; set; }

    [Reactive]
    public string Path { get; set; }
    public Type Type { get; set; }
    
    [Reactive]
    public ObservableCollection<IJsonItem> ChildrenItems { get; set; }
    public IJsonItem? ParentItem { get; set; }
    
    public string JsonType { get; set; }
    
    protected JsonItem(Type type, JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) {
        ParentItem = parent;
        Token = token;
        Path = token.Path.Split('.').Last();
        JsonType = token.Type.ToString();
        Type = type;
        ChildrenItems = new ObservableCollection<IJsonItem>();
        
        //Custom member attribute values
        CustomTooltip = memberInfo?.GetCustomAttributeValueByName<string?>(nameof(Tooltip), nameof(Tooltip.Text), null);
        var customPath = memberInfo?.GetCustomAttributeValueByName<string?>(nameof(SettingName), nameof(SettingName.Name), null);
        if (!customPath.IsNullOrWhitespace()) Path = customPath;
    }
    
    public override string ToString() => Path;
}

public static class JTokenExtension {
    public static Type GetFieldType(this JToken token, Type parentType, out MemberInfo? memberInfo) {
        memberInfo = null;
        
        if (IsCollection(parentType)) {
            return parentType.GenericTypeArguments[0];
        }
        
        switch (token) {
            case JArray:
                foreach (var fieldInfo in parentType.GetFields()) {
                    if (IsCollection(fieldInfo.FieldType)) {
                        memberInfo = fieldInfo;
                        return fieldInfo.FieldType;
                    }
                }
                break;
            case JObject jObject:
                var fieldNames = new List<string>();
                foreach (var (key, _) in jObject) fieldNames.Add(key);

                foreach (var fieldInfo in parentType.GetFields()) {
                    var names = new List<string>(fieldNames);
                    
                    //If all json fields are in type, return the name of the type
                    if (fieldInfo.FieldType.GetFields().All(subField => names.Remove(subField.Name))) {
                        memberInfo = fieldInfo;
                        return fieldInfo.FieldType;
                    }
                }
                break;
            case JProperty jProperty:
                var propertyName = jProperty.Name;

                foreach (var fieldInfo in parentType.GetFields()) {
                    if (fieldInfo.Name == propertyName) {
                        memberInfo = fieldInfo;
                        return fieldInfo.FieldType;
                    }
                }
                
                break;
            case JConstructor:
            case JContainer:
            case JRaw:
            case JValue:
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(token));

        }
        
        return typeof(string);
    }
    
    public static bool IsCollection(Type fieldInfoFieldType) {
        return Enumerable.Contains(fieldInfoFieldType.GetInterfaces(), typeof(ICollection));
    }
}