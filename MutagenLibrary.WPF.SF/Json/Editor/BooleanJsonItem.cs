﻿using System.Reflection;
using MutagenLibrary.WPF.SF.Json.Editor.View;
using Newtonsoft.Json.Linq;
using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public class BooleanJsonItem : JsonItem<bool> {
    public BooleanJsonItem(JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) : base(typeof(bool), token, parent, memberInfo) {
        Value = token.ToObject<bool>();
        View = new BooleanJsonItemView(this);
        
        this.WhenAnyValue(x => x.Value)
            .Subscribe(_ => {
                if (JsonValue == null) return;

                if (Token is JValue value) value.Value = JsonValue;
            });
    }
}