﻿using System.Reflection;
using MutagenLibrary.WPF.SF.Json.Editor.View;
using Newtonsoft.Json.Linq;
using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public class StringJsonItem : JsonItem<string> {
    public StringJsonItem(JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) : base(typeof(string), token, parent, memberInfo) {
        Value = token.ToObject<string>();
        View = new StringJsonItemView(this);

        this.WhenAnyValue(x => x.Value)
            .Subscribe(_ => {
                if (JsonValue == null) return;

                if (Token is JValue value) value.Value = JsonValue;
            });
    }
}
