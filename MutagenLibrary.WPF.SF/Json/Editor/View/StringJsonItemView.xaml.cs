﻿using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor.View; 

public class StringJsonItemViewBase : ReactiveUserControl<StringJsonItem> { }

public partial class StringJsonItemView {
    public StringJsonItemView(StringJsonItem stringJsonItem) {
        InitializeComponent();

        DataContext = ViewModel = stringJsonItem;
    }
}

