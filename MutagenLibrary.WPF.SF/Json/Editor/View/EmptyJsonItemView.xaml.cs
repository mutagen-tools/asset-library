﻿using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor.View; 

public class EmptyJsonItemViewBase : ReactiveUserControl<EmptyJsonItem> { }

public partial class EmptyJsonItemView {
    public EmptyJsonItemView(EmptyJsonItem emptyJsonItem) {
        InitializeComponent();

        DataContext = ViewModel = emptyJsonItem;
    }
}

