﻿using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor.View; 

public class FloatJsonItemViewBase : ReactiveUserControl<FloatJsonItem> { }

public partial class FloatJsonItemView {
    public FloatJsonItemView(FloatJsonItem floatJsonItem) {
        InitializeComponent();

        DataContext = ViewModel = floatJsonItem;
    }
}

