﻿using System.Windows;
using System.Windows.Controls.Primitives;
using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor.View; 

public class FormKeyJsonItemViewBase : ReactiveUserControl<FormKeyJsonItem> { }

public partial class FormKeyJsonItemView {
    public FormKeyJsonItemView(FormKeyJsonItem formKeyJsonItem) {
        InitializeComponent();

        DataContext = ViewModel = formKeyJsonItem;
    }
    
    private void OpenPopup(object sender, RoutedEventArgs e) {
        Popup.IsOpen = true;
        Popup.PlacementTarget  = sender as UIElement;
        Popup.AllowsTransparency = true;
        Popup.PopupAnimation = PopupAnimation.Slide;
    }
}
