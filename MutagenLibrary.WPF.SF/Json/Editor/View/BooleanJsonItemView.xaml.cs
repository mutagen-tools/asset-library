﻿using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor.View; 

public class BooleanJsonItemViewBase : ReactiveUserControl<BooleanJsonItem> { }

public partial class BooleanJsonItemView {
    public BooleanJsonItemView(BooleanJsonItem booleanJsonItem) {
        InitializeComponent();

        DataContext = ViewModel = booleanJsonItem;
    }
}
