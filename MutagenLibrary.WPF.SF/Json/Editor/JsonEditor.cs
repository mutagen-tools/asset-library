﻿using System.IO;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using Noggog;
using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public class JsonEditor : UserControl {
    public const string JsonEditorStyle = "JsonEditorStyle";
    
    public static readonly DependencyProperty JsonFileProperty = DependencyProperty.Register(nameof(JsonFile), typeof(string), typeof(JsonEditor), new FrameworkPropertyMetadata(string.Empty));
    public static readonly DependencyProperty ReadOnlyProperty = DependencyProperty.Register(nameof(ReadOnly), typeof(bool), typeof(JsonEditor), new FrameworkPropertyMetadata(false));
    public static readonly DependencyProperty SaveCommandProperty = DependencyProperty.Register(nameof(SaveCommand), typeof(ICommand), typeof(JsonEditor), new PropertyMetadata(default(ICommand)));
    public static readonly DependencyProperty RefreshCommandProperty = DependencyProperty.Register(nameof(RefreshCommand), typeof(ICommand), typeof(JsonEditor), new PropertyMetadata(default(ICommand)));
    public static readonly DependencyProperty AddCommandProperty = DependencyProperty.Register(nameof(AddCommand), typeof(ICommand), typeof(JsonEditor), new PropertyMetadata(default(ICommand)));
    public static readonly DependencyProperty JsonItemsProperty = DependencyProperty.Register(nameof(JsonItems), typeof(List<IJsonItem>), typeof(JsonEditor), new PropertyMetadata(default(List<IJsonItem>)));
    public static readonly DependencyProperty DuplicateCommandProperty = DependencyProperty.Register(nameof(DuplicateCommand), typeof(ICommand), typeof(JsonEditor), new PropertyMetadata(default(ICommand)));
    public static readonly DependencyProperty RemoveCommandProperty = DependencyProperty.Register(nameof(RemoveCommand), typeof(ICommand), typeof(JsonEditor), new PropertyMetadata(default(ICommand)));
    public static readonly DependencyProperty ClearCommandProperty = DependencyProperty.Register(nameof(ClearCommand), typeof(ICommand), typeof(JsonEditor), new PropertyMetadata(default(ICommand)));
    public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(nameof(SelectedItem), typeof(IJsonItem), typeof(JsonEditor), new PropertyMetadata(default(IJsonItem?)));
    
    public event RoutedEventHandler? JsonSaved;
    
    /// <summary>
    /// file to read json string from 
    /// </summary>
    public string? JsonFile {
        get => (string?) GetValue(JsonFileProperty);
        set => SetValue(JsonFileProperty, value);
    }
    public bool ReadOnly {
        get => (bool) GetValue(ReadOnlyProperty);
        set => SetValue(ReadOnlyProperty, value);
    }
    
    public ICommand SaveCommand {
        get => (ICommand)GetValue(SaveCommandProperty);
        set => SetValue(SaveCommandProperty, value);
    }
    public ICommand RefreshCommand {
        get => (ICommand)GetValue(RefreshCommandProperty);
        set => SetValue(RefreshCommandProperty, value);
    }
    public ICommand AddCommand {
        get => (ICommand)GetValue(AddCommandProperty);
        set => SetValue(AddCommandProperty, value);
    }
    public ICommand ClearCommand {
        get => (ICommand)GetValue(ClearCommandProperty);
        set => SetValue(ClearCommandProperty, value);
    }
    public ICommand DuplicateCommand {
        get => (ICommand)GetValue(DuplicateCommandProperty);
        set => SetValue(DuplicateCommandProperty, value);
    }
    public ICommand RemoveCommand {
        get => (ICommand)GetValue(RemoveCommandProperty);
        set => SetValue(RemoveCommandProperty, value);
    }

    public List<IJsonItem> JsonItems {
        get => (List<IJsonItem>)GetValue(JsonItemsProperty);
        set => SetValue(JsonItemsProperty, value);
    }
    public IJsonItem? SelectedItem {
        get => (IJsonItem?)GetValue(SelectedItemProperty);
        set => SetValue(SelectedItemProperty, value);
    }
    
    public JsonEditor() {
        SetResourceReference(StyleProperty, "JsonEditorStyle");
    }
    
    protected void OnJsonSaved(RoutedEventArgs e) {
        JsonSaved?.Invoke(this, e);
    }
}

public class JsonEditor<TType> : JsonEditor {
    public JsonEditor() {
        this.WhenAnyValue(x => x.JsonFile)
            .Subscribe(UpdateTokens);

        SaveCommand = ReactiveCommand.Create(
            canExecute: this.WhenAnyValue(x => x.ReadOnly)
                .Select(x => !x),
            execute: () => {
                if (JsonFile == null || !JsonItems.Any()) return;

                var file = new FileInfo(JsonFile);
                if (file.Directory is { Exists: false }) {
                    file.Directory?.Create();
                }

                File.WriteAllText(JsonFile, JsonItems[0].Token.ToString());
                OnJsonSaved(new RoutedEventArgs());
            });
        
        RefreshCommand = ReactiveCommand.Create(UpdateTokens);
        
        var isEditableArray = this.WhenAnyValue(x => x.SelectedItem, x => x.ReadOnly)
            .Select(_ => !ReadOnly && SelectedItem != null && SelectedItem.IsCollection());
        var isEditableArrayEntry = this.WhenAnyValue(x => x.SelectedItem, x => x.ReadOnly)
            .Select(_ => !ReadOnly && SelectedItem?.ParentItem != null && SelectedItem.ParentItem.IsCollection());
        
        AddCommand = ReactiveCommand.Create(
            canExecute: isEditableArray,
            execute: () => {
                if (SelectedItem == null) return;

                //Get empty object of list
                var genericType = SelectedItem.Type.GenericTypeArguments[0];

                var obj = genericType == typeof(string) ? string.Empty : Activator.CreateInstance(genericType);
                if (obj == null) return;

                //Add object to tokens hierarchy
                var jObject = JToken.FromObject(obj);
                if (SelectedItem.Token is not JArray parentToken) return;

                parentToken.Add(jObject);
                SelectedItem.ChildrenItems.Add(IJsonItem.Factory(genericType, jObject, SelectedItem));

                //Update relevant items
                if (SelectedItem?.ParentItem == null) return;
                SelectedItem.ParentItem.Update();
                SelectedItem.ParentItem.ChildrenItems.ForEach(c => c.Update());
            });
        
        ClearCommand = ReactiveCommand.Create(
            canExecute: isEditableArray,
            execute: () => SelectedItem?.Clear());
        
        
        DuplicateCommand = ReactiveCommand.Create(
            canExecute: isEditableArrayEntry,
            execute: () => {
                if (SelectedItem?.ParentItem == null) return;
                
                var newToken = SelectedItem.Token.DeepClone();
                var indexOf = SelectedItem.ParentItem.ChildrenItems.IndexOf(SelectedItem);
                if (indexOf == -1) return;
                
                SelectedItem.ParentItem.ChildrenItems.Insert(indexOf + 1, IJsonItem.Factory(SelectedItem.Type, newToken, SelectedItem.ParentItem));
        
                SelectedItem.Token.AddAfterSelf(newToken);
        
                SelectedItem.ParentItem.Update();
                SelectedItem.ParentItem.ChildrenItems.ForEach(c => c.Update());
            }
        );
        
        RemoveCommand = ReactiveCommand.Create(
            canExecute: isEditableArrayEntry,
            execute: () => SelectedItem?.Remove());
    }
    
    /// <summary>
    /// Refreshes the content of the json editor to the content of the current json file
    /// Needs to be called for changes on the current file while it is already loaded in the json editor 
    /// </summary>
    public void UpdateTokens() {
        if (!File.Exists(JsonFile)) return;

        JsonItems = new List<IJsonItem> { IJsonItem.Factory(typeof(TType), JToken.Parse(File.ReadAllText(JsonFile))) };
    }

    public void TryAddJson(object jsonObject) {
        if (!JsonItems.Any() || JsonItems[0].Token is not JArray jArray) return;

        var jToken = JToken.FromObject(jsonObject);
        jArray.Add(jToken);
        JsonItems[0].Update();
    }
}
