﻿using System.Reflection;
using MutagenLibrary.WPF.SF.Json.Editor.View;
using Newtonsoft.Json.Linq;
using ReactiveUI;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public class IntegerJsonItem : JsonItem<int> {
    public IntegerJsonItem(JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) : base(typeof(int), token, parent, memberInfo) {
        Value = token.ToObject<int>();
        View = new IntegerJsonItemView(this);

        this.WhenAnyValue(x => x.Value)
            .Subscribe(_ => {
                if (JsonValue == null) return;

                if (Token is JValue value) value.Value = JsonValue;
            });
    }
}
