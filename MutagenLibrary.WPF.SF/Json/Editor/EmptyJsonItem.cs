﻿using System.Reflection;
using Newtonsoft.Json.Linq;
namespace MutagenLibrary.WPF.SF.Json.Editor;

public class EmptyJsonItem : JsonItem<object> {
    private const int MaxDisplayLength = 75;

    public EmptyJsonItem(Type type, JToken token, IJsonItem? parent = null, MemberInfo? memberInfo = null) : base(type, token, parent, memberInfo) {
        var trimmedToken = token.ToString().Replace(" ", string.Empty).ReplaceLineEndings(string.Empty);
        Value = trimmedToken.Length > MaxDisplayLength ? trimmedToken[..MaxDisplayLength] : trimmedToken;
    }
}
