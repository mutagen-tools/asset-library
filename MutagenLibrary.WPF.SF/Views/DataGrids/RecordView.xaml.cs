﻿namespace MutagenLibrary.WPF.SF.Views.DataGrids; 

public partial class RecordView {
    public RecordView() {
        InitializeComponent();

        Setup(RecordGrid);
    }
}