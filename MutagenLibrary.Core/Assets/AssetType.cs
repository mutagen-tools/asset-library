﻿using Mutagen.Bethesda.Assets;
using Mutagen.Bethesda.Skyrim.Assets;
namespace MutagenLibrary.Core.Assets; 

public abstract class AssetTypeLibrary {
    //Remove first element which is none
    public static readonly IEnumerable<AssetType> AssetTypes = Enum.GetValues<AssetType>().Skip(1);
        
    private static readonly Dictionary<AssetType, List<string>> AssetExtensions = new() {
        { AssetType.Mesh, new List<string> { ".nif" } },
        { AssetType.Texture, new List<string> { ".dds", ".png" } },
        { AssetType.Script, new List<string> { ".psc", ".pex" } },
        { AssetType.Sound, new List<string> { ".wav", ".xwm", ".fuz" } },
        { AssetType.Seq, new List<string> { ".seq" } },
        { AssetType.Tri, new List<string> { ".tri" } },
        { AssetType.Translation, new List<string> { ".dlstrings", ".ilstrings", ".strings" } },
        // { AssetType.Behavior, new List<string> { ".hkx" } },
    };
        
    public static AssetType GetAssetType(string path) {
        var extension = Path.GetExtension(path).ToLower();

        return AssetTypes.FirstOrDefault(type => AssetExtensions[type].Contains(extension));
    }

    public static AssetType MutagenAssetTypeToEnum(IAssetType assetType) {
        switch (assetType) {
            // case SkyrimBehaviorAssetType:
            //     return AssetType.Behavior;
            case SkyrimModelAssetType:
                return AssetType.Mesh;
            case SkyrimTextureAssetType:
                return AssetType.Texture;
            case SkyrimDeformedModelAssetType:
                return AssetType.Tri;
            case SkyrimScriptSourceAssetType:
            case SkyrimScriptCompiledAssetType:
                return AssetType.Script;
            case SkyrimSoundAssetType:
            case SkyrimMusicAssetType:
                return AssetType.Sound;
            case SkyrimSeqAssetType:
                return AssetType.Seq;
            case SkyrimTranslationAssetType:
                return AssetType.Translation;
        }

        return AssetType.None;
    }
}
public enum AssetType {
    None,
    Mesh,
    Texture,
    Script,
    Sound,
    Seq,
    Tri,
    Translation,
    // Behavior
}