﻿using Mutagen.Bethesda;
using Mutagen.Bethesda.Archives;
using MutagenLibrary.Core.Assets;
using Noggog;
namespace MutagenLibrary.Assets.Parser; 

public class BSAParser {
    private readonly IArchiveReader _bsaReader;
        
    public BSAParser(string file) {
        _bsaReader = Archive.CreateReader(GameRelease.SkyrimSE, new FilePath(file));
    }
        
    public HashSet<(string, AssetType)> GetFiles() {
        return new HashSet<(string, AssetType)>(_bsaReader.Files.Select(file => (file.Path, AssetTypeLibrary.GetAssetType(file.Path))));
    }

    public IArchiveFile GetFile(string path) {
        return _bsaReader.Files.First(file => file.Path.Equals(path, StringComparison.OrdinalIgnoreCase));
    }
}