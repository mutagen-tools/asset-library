﻿using nifly;
namespace MutagenLibrary.Assets.Parser; 

public static class NifParser {
    public static IEnumerable<string> ParseNIF(string file) {
        if (!File.Exists(file)) return new List<string>();

        using var nif = new NifFile();
        nif.Load(file);

        var uniqueAssets = new HashSet<string>();
        if (!nif.IsValid()) return uniqueAssets;

        var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(nif.GetHeader()));
        for (uint blockId = 0; blockId<blockCache.Header.GetNumBlocks(); ++blockId) {
            using var shaderTextureSet = blockCache.EditableBlockById<BSShaderTextureSet>(blockId);
            if (shaderTextureSet != null) {
                foreach (var niString in shaderTextureSet.textures.items()) {
                    AddAsset(niString);
                }
            } else {
                var effectShader = blockCache.EditableBlockById<BSEffectShaderProperty>(blockId);
                if (effectShader != null) {
                    AddAsset(effectShader.greyscaleTexture);
                    AddAsset(effectShader.lightingTexture);
                    AddAsset(effectShader.normalTexture);
                    AddAsset(effectShader.reflectanceTexture);
                    AddAsset(effectShader.sourceTexture);
                    AddAsset(effectShader.emitGradientTexture);
                    AddAsset(effectShader.envMapTexture);
                    AddAsset(effectShader.envMaskTexture);
                } else {
                    var shaderNoLighting = blockCache.EditableBlockById<BSShaderNoLightingProperty>(blockId);
                    if (shaderNoLighting != null) {
                        AddAsset(shaderNoLighting.baseTexture);
                    }
                }
            }
        }
        
        void AddAsset(NiString? asset) {
            if (asset == null) return;

            var assetString = asset.get();
            if (!string.IsNullOrEmpty(asset.get())) {
                uniqueAssets.Add(assetString);
            }
        }

        return uniqueAssets;
    }
}