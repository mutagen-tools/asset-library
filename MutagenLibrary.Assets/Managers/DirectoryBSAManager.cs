﻿using MutagenLibrary.Assets.Parser;
using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Managers;

public class DirectoryBSAManager : BSAManager {
    public DirectoryBSAManager(string file) : base(file) {}
    
    protected override string ManagerType => "BSA";
    protected override bool CacheAssets => true;
    
    protected override void ParseBSA(BSAParser bsaParser) {
        foreach (var (file, assetType) in bsaParser.GetFiles()) {
            AddAsset(file, assetType, BSA, bsaParser.GetFile(file).Size);
        }
    }
    
    public override void ParseAsset(string path, AssetType assetType) {
        throw new NotImplementedException();
    }
}