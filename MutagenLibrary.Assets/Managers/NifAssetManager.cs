﻿using System.IO.Abstractions;
using MutagenLibrary.Assets.Parser;
using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Managers; 

public class NifAssetManager : FileStructureManager {
    public NifAssetManager(string directory, bool bsaParsing, IFileSystem? fileSystem = null) : base(directory, bsaParsing, fileSystem) {}
    public NifAssetManager(string directory, bool bsaParsing, List<string> ignoredDirectories, IFileSystem? fileSystem = null) : base(directory, bsaParsing, ignoredDirectories, fileSystem) {}
    
    protected override string ManagerType => "NifAsset";
    protected override bool CacheAssets => true;

    protected override void ParseFile(string file) {
        var assetType = AssetTypeLibrary.GetAssetType(file);
            
        if (assetType == AssetType.Mesh) {
            ParseAsset(file, assetType);
        } else if (Path.GetExtension(file).Equals(ArchiveExtension, StringComparison.OrdinalIgnoreCase)) {
            var bsaManager = new NifAssetBSAManager(file);
            SubManagers.Add(bsaManager);
            if (BSAParsing) {
                bsaManager.ParseAssets();                        
            }
        }
    }
    
    public override void ParseAsset(string path, AssetType assetType) {
        foreach (var outPath in NifParser.ParseNIF(path)) {
            AddAsset(outPath, AssetTypeLibrary.GetAssetType(outPath), path);
        }
    }
}