﻿using System.IO.Abstractions;
using Elscrux.Notification;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Archives;
namespace MutagenLibrary.Assets.Managers; 

public abstract class FileStructureManager : AssetManager {
    protected static readonly string ArchiveExtension = Archive.GetExtension(GameRelease.SkyrimSE);

    protected readonly IFileSystem FileSystem;
    
    protected readonly string Directory;
    protected bool BSAParsing;
    protected readonly List<string> IgnoredDirectories = new();

    public override string Name => Directory;
    protected override DateTime Time => GetTime(Directory);
    
    protected FileStructureManager(string directory, bool bsaParsing, IFileSystem? fileSystem = null) {
        FileSystem = fileSystem ?? new FileSystem();
        if (!FileSystem.Directory.Exists(directory)) throw new IOException($"Directory {directory} not found");
        
        Directory = directory;
        BSAParsing = bsaParsing;
    }

    protected FileStructureManager(string directory, bool bsaParsing, List<string> ignoredDirectories, IFileSystem? fileSystem = null) : this(directory, bsaParsing, fileSystem) {
        IgnoredDirectories = ignoredDirectories;
    }

    protected override void ParseAssetsImplementation(INotifier? notifier = null) {
        ParseAssetsRec(Directory);
    }

    private void ParseAssetsRec(string directory) {
        if (IgnoredDirectories.Any(NormalizePath(directory).StartsWith)) return;
        
        foreach (var file in FileSystem.Directory.EnumerateFiles(directory)) {
            ParseFile(file);
        }
        
        foreach (var subdirectory in FileSystem.Directory.GetDirectories(directory)) {
            ParseAssetsRec(subdirectory);
        }
    }

    protected abstract void ParseFile(string file);

    protected string NormalizePath(string path) {
        //Remove directory and \ character
        return path.Length <= Name.Length ? string.Empty : path[(Name.Length + 1)..];

    }

    private DateTime GetTime(string directory) {
        var mostRecent = DateTime.MinValue;
            
        foreach (var file in FileSystem.Directory.EnumerateFiles(directory)) {
            var time = File.GetLastWriteTime(file);
            if (time > mostRecent) {
                mostRecent = time;
            }
        }
            
        foreach (var subdirectory in FileSystem.Directory.GetDirectories(directory)) {
            if (IgnoredDirectories.Any(subdirectory[Name.Length..].StartsWith)) continue;

            var directoryInfo = new DirectoryInfo(subdirectory);
            if (directoryInfo.LastWriteTime > mostRecent) {
                mostRecent = directoryInfo.LastWriteTime;
            }
            
            var time = GetTime(subdirectory);
            if (time > mostRecent) {
                mostRecent = time;
            }
        }
            
        return mostRecent;
    }
    
    public void EnableBSAParsing() {
        BSAParsing = true;
    }
}
