﻿using System.Globalization;
using System.Text.RegularExpressions;
using Elscrux.Notification;
using Ionic.Zip;
using Ionic.Zlib;
using MutagenLibrary.Assets.Rules;
using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Managers;

public abstract class AssetManager {
    /*====================================================
        Caching
    ====================================================*/
    private const string CacheFolder = "MutagenCache";
    private const string CacheExtension = "cache";
    private const string TempCacheExtension = "temp";
    private static readonly Regex IllegalFileNameRegex = new(@"[:*?""<>|\[\]]");
    private static readonly Version CacheVersion = new(2, 0, 0, 0);

    /// <summary>
    /// Asset for all asset types mapped to a list of their usages
    /// </summary>
    protected readonly Dictionary<AssetType, Dictionary<Asset, HashSet<string>>> Assets = AssetTypeLibrary.AssetTypes.ToDictionary(
        type => type,
        _ => new Dictionary<Asset, HashSet<string>>()
    );

    protected readonly List<AssetManager> SubManagers = new();

    private bool _requiresAssets = true;
    public bool RequiresAssets {
        get => _requiresAssets;
        set {
            _requiresAssets = value;
            foreach (var subManager in SubManagers) subManager.RequiresAssets = value;
        }
    }
    
    public abstract string Name { get; }
    protected abstract string ManagerType { get; }
    protected abstract DateTime Time { get; }
    
    public abstract void ParseAsset(string path, AssetType assetType);

    protected abstract void ParseAssetsImplementation(INotifier? notifier = null);

    public void ParseAssets(INotifier? notifier = null) {
        var notify = new SingleNotifier(notifier, $"Parsing Assets of {Name}");
        try {
            notify.Start();
            
            //If caching is enabled and cache is available, load it and return
            if (CacheAssets && LoadAssetCache(notifier)) return;

            //Otherwise parse assets
            ParseAssetsImplementation(notifier);

            //If caching is enabled, cache the assets
            if (CacheAssets) SaveAssetCache(notifier);
        } finally {
            notify.Stop();
        }
    }

    public void ForceParseSubManagers() {
        foreach (var bsaManager in SubManagers) bsaManager.ParseAssets();
    }

    protected void AddAsset(string path, AssetType assetType, string origin, long fileSize = 0) {
        if (string.IsNullOrEmpty(path)) return;
        if (assetType == AssetType.None) return;

        foreach (var outPath in AssetRule.ApplyAssetRules(path, assetType)) {
            var asset = new Asset(outPath, fileSize);
            if (Assets[assetType].TryGetValue(asset, out var usages)) usages.Add(origin);
            else Assets[assetType].Add(asset, new HashSet<string> { origin });
        }
    }

    public void RemoveAsset(Asset asset) {
        var assetType = AssetTypeLibrary.GetAssetType(asset.Path);
        Assets[assetType].Remove(asset);
    }

    public IEnumerable<Asset> GetRequiredAssets(AssetType assetType) {
        return _requiresAssets ? GetAssets(assetType) : new List<Asset>();
    }

    public IEnumerable<Asset> GetAssets(AssetType assetType) {
        var assets = new List<Asset>();

        //Add sub managers
        foreach (var assetManager in SubManagers) assets.AddRange(assetManager.GetAssets(assetType));

        //Add self
        assets.AddRange(Assets[assetType].Keys);

        return assets;
    }

    public bool HasAsset(Asset asset, AssetType assetType) {
        return Assets.ContainsKey(assetType) && (Assets[assetType].ContainsKey(asset) || SubManagers.Any(manager => manager.HasAsset(asset, assetType)));
    }

    /// <summary>
    /// Retrieve references of this asset in this asset manager.
    /// If you have the asset type available, use this to reduce runtime.
    /// </summary>
    /// <param name="asset">Asset to retrieve references for</param>
    /// <param name="assetType">Asset type of asset</param>
    /// <returns>String representation of the entity referencing asset</returns>
    public IEnumerable<string> GetUsages(Asset asset, AssetType assetType) {
        if (!Assets.ContainsKey(assetType)) return new List<string>();
        
        var usages = new List<string>();

        //Add sub managers
        foreach (var assetManager in SubManagers) usages.AddRange(assetManager.GetUsages(asset, assetType));

        //Add self
        var assets = Assets[assetType];
        if (assets.TryGetValue(asset, out var newUsages)) usages.AddRange(newUsages);

        return usages;
    }
    
    /// <summary>
    /// Retrieve references of this asset in this asset manager.
    /// </summary>
    /// <param name="asset">Asset to retrieve references for</param>
    /// <returns>String representation of the entity referencing asset</returns>
    public IEnumerable<string> GetUsages(Asset asset) {
        return GetUsages(asset, AssetTypeLibrary.GetAssetType(asset.Path));
    }

    protected abstract bool CacheAssets { get; }
    protected string CacheDirectory => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), CacheFolder, ManagerType);
    private string Cache => Path.Combine(CacheDirectory, $"{IllegalFileNameRegex.Replace(Name, string.Empty)}.{CacheExtension}");
    private string TempCache => Path.Combine(CacheDirectory, $"{IllegalFileNameRegex.Replace(Name, string.Empty)}.{TempCacheExtension}");

    /// <summary>
    /// Load cache from file if possible, return true on success, return false if cache not up to date or nonexistent
    /// </summary>
    /// <param name="notifier"></param>
    /// <returns>true if cache was read successfully</returns>
    /// <exception cref="BadReadException"></exception>
    private bool LoadAssetCache(INotifier? notifier = null) {
        //Only regenerate cache if necessary
        if (!File.Exists(Cache)) return false;

        try {
            var level = notifier?.PushLevel();
            
            //Create temporary uncompressed cache file
            notifier?.Notify("Decompressing Cache", level);
            using (var fileStream = File.OpenRead(Cache)) {
                using (var zip = new GZipStream(fileStream, CompressionMode.Decompress)) {
                    using (var tempFileStream = File.OpenWrite(TempCache)) {
                        zip.CopyTo(tempFileStream);
                    }
                }
            }

            //Search for reference in cache
            using (var reader = new BinaryReader(File.OpenRead(TempCache))) {
                try {
                    //Tool updated -> update cache
                    var version = Version.Parse(reader.ReadString());
                    if (version != CacheVersion) return false;

                    //Cache is not up to date -> update cache
                    if (!DateTime.TryParse(reader.ReadString(), out var cacheTime) || cacheTime < Time) return false;

                    Assets.Clear();

                    var formCount = reader.ReadInt32();
                    var countingNotifier = new CountingNotifier(notifier, formCount, level);
                    countingNotifier.Start("Reading Cache");
                    for (var i = 0; i < formCount; i++) {
                        countingNotifier.NextStep();
                        
                        var assetTypeString = reader.ReadString();
                        if (Enum.TryParse<AssetType>(assetTypeString, out var assetType)) {
                            var assetCount = reader.ReadInt32();

                            var assetPaths = new Dictionary<Asset, HashSet<string>>();
                            for (var j = 0; j < assetCount; j++) {
                                var assetPath = reader.ReadString();
                                var assetUsageCount = reader.ReadInt32();
                                var assetUsages = new HashSet<string>();
                                for (var k = 0; k < assetUsageCount; k++) assetUsages.Add(reader.ReadString());

                                assetPaths.Add(new Asset(assetPath), assetUsages);
                            }

                            Assets.Add(assetType, assetPaths);
                        } else {
                            throw new BadReadException($"{assetTypeString} is not a valid asset type");
                        }
                    }
                } finally {
                    reader.Close();
                }
            }
            
            return true;
        } finally {
            notifier?.PopLevel();
            
            //Delete temporary cache file
            File.Delete(TempCache);
        }
    }

    /// <summary>
    /// Builds Reference cache of one mod
    /// </summary>
    /// <param name="notifier"></param>
    public void SaveAssetCache(INotifier? notifier = null) {
        if (!CacheAssets) return;
        
        //Write assets to cache file
        var info = new FileInfo(Cache);
        info.Directory?.Create();
        using var fileStream = File.OpenWrite(info.FullName);
        using var zip = new GZipStream(fileStream, CompressionMode.Compress);
        var writer = new BinaryWriter(zip);
        
        writer.Write(CacheVersion.ToString());
        writer.Write(DateTime.Now.ToString(CultureInfo.InvariantCulture));
        writer.Write(Assets.Count);
        
        var countingNotifier = new CountingNotifier(notifier, Assets.Count);
        countingNotifier.Start("Saving Cache");
        foreach (var (assetType, assets) in Assets) {
            countingNotifier.NextStep();
            
            writer.Write(assetType.ToString());
            writer.Write(assets.Count);
            foreach (var (name, usages) in assets) {
                writer.Write(name.Path);
                writer.Write(usages.Count);
                foreach (var usage in usages) writer.Write(usage);
            }
        }
    }
}