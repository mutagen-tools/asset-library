﻿using MutagenLibrary.Assets.Parser;
using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Managers; 

public class NifAssetBSAManager : BSAManager {
    public NifAssetBSAManager(string file) : base(file) {}

    protected override string ManagerType => "BSANifAsset";
    protected override bool CacheAssets => true;
    
    protected override void ParseBSA(BSAParser bsaParser) {
        foreach (var (file, assetType) in bsaParser.GetFiles()) {
            if (assetType != AssetType.Mesh) continue;
            
            //Create temp file and copy file from bsa to it
            var tempFile = new FileInfo(Path.Combine(CacheDirectory, file));
            tempFile.Directory?.Create();
            var bsaStream = File.Create(tempFile.FullName);
            bsaParser.GetFile(file).AsStream().CopyTo(bsaStream);
            bsaStream.Close();
            
            //Parse temp file as nif and delete it afterwards 
            ParseAsset(tempFile.FullName, AssetType.Mesh);
            File.Delete(tempFile.FullName);
        }
    }

    public override void ParseAsset(string path, AssetType assetType) {
        if (assetType != AssetType.Mesh) return;
        foreach (var outPath in NifParser.ParseNIF(path)) {
            AddAsset(outPath, AssetTypeLibrary.GetAssetType(outPath), BSA);
        }
    }
}