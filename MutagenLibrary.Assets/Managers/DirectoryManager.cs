﻿using System.IO.Abstractions;
using MutagenLibrary.Core.Assets;
namespace MutagenLibrary.Assets.Managers; 

public class DirectoryManager : FileStructureManager {
    public DirectoryManager(string directory, bool bsaParsing, IFileSystem? fileSystem = null) : base(directory, bsaParsing, fileSystem) { }
    public DirectoryManager(string directory, bool bsaParsing, List<string> ignoredDirectories, IFileSystem? fileSystem = null) : base(directory, bsaParsing, ignoredDirectories, fileSystem) { }

    protected override string ManagerType => "Directory";
    protected override bool CacheAssets => false;
    
    protected override void ParseFile(string file) {
        var assetType = AssetTypeLibrary.GetAssetType(file);
        var extension = Path.GetExtension(file);
                
        if (assetType != AssetType.None) {
            ParseAsset(file, assetType);
        } else if (extension.Equals(ArchiveExtension, StringComparison.OrdinalIgnoreCase)) {
            //BSA
            var bsaAssetManager = new DirectoryBSAManager(file);
            SubManagers.Add(bsaAssetManager);
            if (BSAParsing) {
                bsaAssetManager.ParseAssets();                        
            }
        }
    }
    
    public override void ParseAsset(string path, AssetType assetType) {
        AddAsset(NormalizePath(path), assetType, Name, new FileInfo(path).Length);
    }
}