﻿using Elscrux.Notification;
using Mutagen.Bethesda;
using Mutagen.Bethesda.Archives;
using MutagenLibrary.Assets.Parser;
namespace MutagenLibrary.Assets.Managers; 

public abstract class BSAManager : AssetManager {
    protected readonly string BSA;
    
    public override string Name => BSA;
    protected override DateTime Time => File.GetLastWriteTime(BSA);
    
    protected BSAManager(string bsa) {
        if (!Archive.GetExtension(GameRelease.SkyrimSE).Equals(Path.GetExtension(bsa), StringComparison.OrdinalIgnoreCase)) throw new ArgumentException($"{bsa} is no bsa file");
        
        BSA = bsa;
    }

    protected override void ParseAssetsImplementation(INotifier? notifier = null) {
        ParseBSA(new BSAParser(BSA));
    }

    protected abstract void ParseBSA(BSAParser bsaParser);
}
