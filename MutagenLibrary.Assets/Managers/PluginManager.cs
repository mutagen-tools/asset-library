﻿using System.Globalization;
using Elscrux.Notification;
using Mutagen.Bethesda.Environments;
using Mutagen.Bethesda.Plugins;
using Mutagen.Bethesda.Plugins.Assets;
using Mutagen.Bethesda.Plugins.Records;
using Mutagen.Bethesda.Skyrim.Assets;
using MutagenLibrary.Core.Assets;
using MutagenLibrary.Core.Plugins;
namespace MutagenLibrary.Assets.Managers; 

public class PluginManager : AssetManager {
    private readonly IGameEnvironment _environment;
    public static readonly string Separator = CultureInfo.CurrentCulture.TextInfo.ListSeparator;
    
    public PluginManager(IGameEnvironment environment, string name) {
        _environment = environment;
        Name = name;
    }
    
    public override string Name { get; }
    protected override string ManagerType => "Plugin";
    protected override DateTime Time => File.GetLastWriteTime(Path.Combine(_environment.DataFolderPath.Path, Name));
    protected override bool CacheAssets => true;
    public bool SkipResolvedAssets { get; set; } = false;
    
    public override void ParseAsset(string path, AssetType assetType) {
        throw new NotImplementedException();
    }

    protected override void ParseAssetsImplementation(INotifier? notifier = null) {
        var mod = _environment.LoadOrder.ListedOrder.First(l => l.Mod?.ModKey.FileName == Name).Mod;
        if (mod == null) return;

        foreach (var assetLink in GetModAssets(mod)) {
            AddAsset(assetLink.DataRelativePath, AssetTypeLibrary.MutagenAssetTypeToEnum(assetLink.Type), mod.ModKey.Name);
        }

        var countingNotifier = new CountingNotifier(notifier, (int) mod.GetRecordCount());
        countingNotifier.Start($"Parsing Records in {Name}");
        if (SkipResolvedAssets) {
            foreach (var record in mod.EnumerateMajorRecords()) {
                countingNotifier.NextStep();
                var name = $"{record.FormKey.ToString()}{Separator}{record.EditorID}";
                foreach (var assetLink in record.EnumerateInferredAssetLinks().Where(l => !l.IsNull)) {
                    AddAsset(assetLink.DataRelativePath, AssetTypeLibrary.MutagenAssetTypeToEnum(assetLink.Type), name);
                }
            }
        } else {
            var immutableAssetLinkCache = _environment.LinkCache.CreateImmutableAssetLinkCache();
            foreach (var record in mod.EnumerateMajorRecords()) {
                countingNotifier.NextStep();
                var name = $"{record.FormKey.ToString()}{Separator}{record.EditorID}";
                foreach (var assetLink in record.EnumerateAllAssetLinks(immutableAssetLinkCache).Where(l => !l.IsNull)) {
                    AddAsset(assetLink.DataRelativePath, AssetTypeLibrary.MutagenAssetTypeToEnum(assetLink.Type), name);
                }
            }
        }
    }

    private static IEnumerable<IAssetLink> GetModAssets(IModGetter mod) {
        if (mod.UsingLocalization) yield break;

        var modName = mod.ModKey.Name;
        foreach (var language in SkyrimTranslationAssetType.Languages) {
            foreach (var translationExtension in SkyrimTranslationAssetType.Instance.FileExtensions) {
                yield return new AssetLink<SkyrimTranslationAssetType>($"{modName}_{language}.{translationExtension}");
            }
        }
    }

    /// <summary>
    /// Retrieves the assets that are only referenced in the records parameter
    /// Note that only records that are part of this mod will be noticed
    /// </summary>
    /// <returns>Set of assets that are uniquely used in records</returns>
    public HashSet<Asset> GetUniqueAssets(HashSet<FormKey> records) {
        var referencedAssets = new HashSet<Asset>();
        
        foreach (var assets in Assets.Values) {
            foreach (var (asset, usages) in assets) {
                if (usages.All(usage => records.Contains(FormKey.Factory(usage.Split(Separator)[0])))) {
                    referencedAssets.Add(asset);
                }
            }
        }

        return referencedAssets;
    }

    /// <summary>
    /// Retrieve references of this asset in this asset manager.
    /// </summary>
    /// <param name="asset">Asset to retrieve references for</param>
    /// <param name="assetType">Asset type of asset</param>
    /// <returns>List of (FormKeys, EditorIDs) that are referencing asset</returns>
    public new IEnumerable<(string, string)> GetUsages(Asset asset, AssetType assetType) {
        return ((AssetManager) this).GetUsages(asset, assetType)
            .Select(usage => usage.Split(Separator))
            .Select(splitUsage => (splitUsage[0], splitUsage[1]))
            .ToList();
    }
}