﻿namespace MutagenLibrary.Assets; 

public record Asset(string Path, long Size = 0) : IComparable<Asset> {
    private const StringComparison PathComparison = StringComparison.OrdinalIgnoreCase;
    private static readonly StringComparer PathComparer = StringComparer.FromComparison(PathComparison);
    
    public string Path { get; } = Path;
    public long Size { get; } = Size;

    public virtual bool Equals(Asset? other) {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        
        return PathComparer.Equals(Path, other.Path);
    }

    public override int GetHashCode() {
        return PathComparer.GetHashCode(Path);
    }

    public int CompareTo(Asset? other) {
        return PathComparer.Compare(other?.Path, Path);
    }
}