﻿using MutagenLibrary.Core.Assets;
using Noggog;
namespace MutagenLibrary.Assets.Rules; 

public abstract class AssetRule : IComparable<AssetRule> {
    /** Asset will call apply rule if it's asset type or AssetType.None is part of the AssetType list */
    protected abstract List<AssetType> ValidAssetTypes { get; }
    
    /** Load rules with high priorities first usually 0 - 1023 */
    protected abstract int Priority { get; }
        
    /** Apply rule specific to class */
    protected abstract IEnumerable<string> ApplyRule(string path);
        
    public int CompareTo(AssetRule? other) {
        return other == null ? 1 : other.Priority.CompareTo(Priority);
    }

    private static readonly Dictionary<AssetType, List<AssetRule>> AssetRules = GetAssetRules();

    private static Dictionary<AssetType, List<AssetRule>> GetAssetRules() {
        var output = new Dictionary<AssetType, List<AssetRule>>();
            
        //Get all asset rules
        var allAssetRules = typeof(AssetRule)
            .GetSubclassesOf()
            .Select(Activator.CreateInstance)
            .NotNull()
            .Cast<AssetRule>()
            .ToList();

        //Filter asset rules for each asset type
        foreach (var assetType in AssetTypeLibrary.AssetTypes) {
            var assetRules = allAssetRules
                .Where(assetRule => assetRule.ValidAssetTypes.Contains(assetType) || assetRule.ValidAssetTypes.Contains(AssetType.None))
                .ToList();

            //Sort asset rules by index
            assetRules.Sort();

            output.Add(assetType, assetRules);
        }

        return output;
    }
        
    public static IEnumerable<string> ApplyAssetRules(string path, AssetType assetType) {
        var paths = new List<string> { path };
        foreach (var assetRule in AssetRules[assetType]) {
            var updatedPaths = new List<string>();
            foreach (var assetPath in paths) {
                updatedPaths.AddRange(assetRule.ApplyRule(assetPath));
            }

            paths = updatedPaths;
        }

        return paths;
    }
}