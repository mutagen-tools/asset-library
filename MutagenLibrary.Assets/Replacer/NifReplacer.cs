﻿using nifly;
namespace MutagenLibrary.Assets.Replacer; 

public static class NifReplacer {
    public static void ReplaceAssets(string file, string origin, string target) {
        if (!File.Exists(file)) return;

        using var nif = new NifFile();
        nif.Load(file);

        if (!nif.IsValid()) return;

        var modifiedNif = false;
        
        var originFileName = Path.GetFileName(origin);
        var niHeader = nif.GetHeader();
        var blockCache = new niflycpp.BlockCache(niflycpp.BlockCache.SafeClone<NiHeader>(niHeader));
        for (uint blockId = 0; blockId<blockCache.Header.GetNumBlocks(); ++blockId) {
            using var shaderTextureSet = blockCache.EditableBlockById<BSShaderTextureSet>(blockId);
            if (shaderTextureSet != null) {
                var items = shaderTextureSet.textures.items();
                var occurenceIndices = new List<int>();
                for (var index = 0; index < items.Count; index++) {
                    var niString = items[index];
                    if (niString == null) return;
                    
                    if (originFileName.Equals(Path.GetFileName(niString.get()), StringComparison.OrdinalIgnoreCase)) {
                        occurenceIndices.Add(index);
                    }
                }

                if (occurenceIndices.Count <= 0) continue;
                
                var vectorNiString = new vectorNiString();
                for (var i = 0; i < 9; i++) {
                    vectorNiString.Add(occurenceIndices.Contains(i) ? new NiString(target) : items[i]);
                }
                
                using var textureWrapper = new NiStringVector();
                textureWrapper.SetItems(vectorNiString);
                shaderTextureSet.textures = textureWrapper;
                niHeader.ReplaceBlock(blockId, shaderTextureSet);

                modifiedNif = true;
            } else {
                var effectShader = blockCache.EditableBlockById<BSEffectShaderProperty>(blockId);
                
                if (effectShader != null) {
                    var modifiedEffectShader = false;
                    
                    if (originFileName.Equals(Path.GetFileName(effectShader.greyscaleTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.greyscaleTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }
                    if (originFileName.Equals(Path.GetFileName(effectShader.lightingTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.lightingTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }
                    if (originFileName.Equals(Path.GetFileName(effectShader.normalTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.normalTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }
                    if (originFileName.Equals(Path.GetFileName(effectShader.reflectanceTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.reflectanceTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }
                    if (originFileName.Equals(Path.GetFileName(effectShader.sourceTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.sourceTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }
                    if (originFileName.Equals(Path.GetFileName(effectShader.emitGradientTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.emitGradientTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }
                    if (originFileName.Equals(Path.GetFileName(effectShader.envMapTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.envMapTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }
                    if (originFileName.Equals(Path.GetFileName(effectShader.envMaskTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        effectShader.envMaskTexture = new NiString(target);
                        modifiedEffectShader = true;
                    }

                    if (modifiedEffectShader) {
                        niHeader.ReplaceBlock(blockId, effectShader);
                        modifiedNif = true;
                    }
                } else {
                    var shaderNoLighting = blockCache.EditableBlockById<BSShaderNoLightingProperty>(blockId);
                    if (shaderNoLighting != null && originFileName.Equals(Path.GetFileName(shaderNoLighting.baseTexture.get()), StringComparison.OrdinalIgnoreCase)) {
                        shaderNoLighting.baseTexture = new NiString(target);
                        niHeader.ReplaceBlock(blockId, shaderNoLighting);
                        
                        modifiedNif = true;
                    }
                }
            }
        }
        
        if (modifiedNif) nif.Save(file);
    }
}