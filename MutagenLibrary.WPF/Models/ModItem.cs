using Mutagen.Bethesda.Plugins;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenLibrary.WPF.Models;

public class ModItem : ReactiveObject, ISelectable {
    public static readonly ModKey NewMod = new();

    public ModItem(ModKey modKey) => ModKey = modKey;

    [Reactive]
    public bool IsSelected { get; set; }
    
    [Reactive]
    public ModKey ModKey { get; protected set; }

    public override string ToString() => ModKey == NewMod ? "< new plugin >" : ModKey.FileName;
}