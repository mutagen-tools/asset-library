﻿using Mutagen.Bethesda.Plugins.Records;
using Noggog;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
namespace MutagenLibrary.WPF.Models;

public class RecordItem : ReactiveObject, ISelectable  {
    public IMajorRecordGetter Record { get; protected set; }

    [Reactive]
    public bool IsSelected { get; set; }
    
    [Reactive]
    public string FormID { get; protected set; }
    
    [Reactive]
    public string EditorID { get; protected set; }
    
    [Reactive]
    public string RecordType { get; protected set; }
    
    public RecordItem(IMajorRecordGetter record) {
        Record = record;
        FormID = record.FormKey.ToString();
        EditorID = record.EditorID ?? string.Empty;
        RecordType = Record.Registration.ClassType.Name;
    }

    public void SetRecord(IMajorRecordGetter record) {
        Record = record;
        FormID = record.FormKey.ToString();
        EditorID = record.EditorID ?? string.Empty;
        RecordType = Record.Registration.ClassType.Name;
    }
}